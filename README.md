# Pragma Design System
## Librería de Componentes en Stencil

¡Bienvenido! Esta es la documentación de la librería **Pragma Design System** desarrollada en Stencil. Aquí encontrarás información sobre la estructura del proyecto, cómo trabajar con los diferentes proyectos incluidos en el monorepo y cómo probar los componentes en diferentes frameworks.

## Estructura del Proyecto

El repositorio está organizado como un monorepo que contiene varios proyectos relacionados. Aquí está la estructura general del proyecto:
```
├── stencil-library/
├── angular-workspace/ 
├── react-library/ 
├── starter-kits/
│ ├── angular-app/ 
│ └── react-app/ 
```

**Instalacion de las dependencias** Ejecutando `npm install`.

## Proyecto Stencil (`stencil-library`)

En este proyecto, se desarrollan los componentes utilizando Stencil. Los componentes se crean siguiendo las mejores prácticas de Stencil y se pueden encontrar en la carpeta `src/components`. Para construir y compilar los componentes, sigue estos pasos:


2. **Desarrolla tus componentes** En la carpeta `src/components` estan los componentes a desarrollar.
3. **Compila la libreria web component**: Ejecuta `npm run build.stencil`.

## Proyectos para Integración con Frameworks

### Angular Workspace (`angular-workspace`)

Wrapper de web components para su uso en Angular 

Este proyecto genera un wrapper de web components para su uso en Angular. Los pasos son los siguientes:

1. **Compila los componentes**: Ejecuta `npm run build.angular` para compilar el wrapper de angular.

### React Library (`react-library`)

Este proyecto genera un wrapper de web components para su uso en React. Los pasos son los siguientes:

1. **Compila los componentes**: Ejecuta `npm run build.react` para compilar el wrapper de React.

## Starter Kits (`starter-kits`)

En esta carpeta, encontrarás proyectos de ejemplo para probar los componentes en Angular y React. Sigue estos pasos para utilizarlos:

1. **Instala las dependencias**: Ejecuta los siguientes comando `npm run install.angular` y `npm run install.react`.
2. **Ejecutar app**: Ejecuta los siguientes comandos `npm run start.angular` o `npm run start.react`, segun el framwork que vas a utilizar.
3. Utiliza los componentes adaptados de la librería en la aplicación de ejemplo.


## Iniciar proyectos

Para ejecutar los servidores desde la raiz del proyecto ejecutamos los siguientes comandos:

**Iniciar proyecto de Stencil**

`npm run start.stencil`

**Iniciar proyecto de Angular-app**

`npm run start.angular`

**Iniciar proyecto de React-app**

`npm run start.react` 

## Publicar librerias

### Paso 1: Preparar la Librería

Antes de publicar la librería, asegúrate de que todo esté en orden y listo para ser compartido:

1. Verificación de Componentes: Asegúrate de que los componentes estén completamente desarrollados, probados y listos para ser utilizados por otros desarrolladores.

2. Versionamiento: Decide la versión de la librería que deseas publicar. Las versiones se cambian en los siguientes packages.jon:

    - `stencil-library/package.json`

    - `angular-workspace/projects/component-library/package.json`

    - `react-library/package.json`

    **Es importante cambiar la versión de la librería una vez estes listo para publicar la nueva versión.**

### Paso 2: Compilar los Componentes

Debes asegurarte de que los componentes estén compilados y listos para ser utilizados por diferentes frameworks, desde la raíz del proyecto ejecuta los siguientes scripts para hacer build a las librerias:

- `npm run build.stencil`

- `npm run build.react`

- `npm run build.angular`

### Paso 3: Publicar en NPM

Para poder publicar debes estar logueado en tu cuenta de npm ejecutando `npm login`, solo las cuentas que tengas permiso de publicar dentro de la organización [@pragma-dev-kit](https://www.npmjs.com/package/@pragma-dev-kit/pragma-ds) podrán hacerlo.

1. **Publicar la libreria de Stencil**: debemos de ir al siguiente ruta: `stencil-library/dist` y ejecutar el siguiente comando `npm publish`

1. **Publicar la libreria de React**: debemos de ir al siguiente ruta: `react-library/dist` y ejecutar el siguiente comando `npm publish`

1. **Publicar la libreria de Angular**: debemos de ir al siguiente ruta: `angular-workspace/dist/component-library` y ejecutar el siguiente comando `npm publish`

### ¡Listo para Compartir!

¡Felicidades! Ahora has publicado exitosamente tu librería de componentes en Stencil y está disponible para otros desarrolladores. Asegúrate de mantener la documentación actualizada y estar atento a cualquier pregunta o problema que puedan tener los usuarios.

Recuerda que la colaboración y el feedback son esenciales para mejorar tu librería y hacerla más útil para la comunidad de desarrollo.

## Documentacion de componentes

Para la documentación estamos utilizando Storybook, es una herramienta de desarrollo utilizada principalmente en el desarrollo de interfaces de usuario (UI) para aplicaciones web. Proporciona un entorno aislado en el que los desarrolladores pueden construir y visualizar componentes de manera individual, permitiendo ver cómo se comportan en diferentes estados y variaciones sin necesidad de ejecutar la aplicación completa.

## Iniciar Storybook

Antes de iniciar Stroybook primero se debe compilar la libreria de Stencil con el siguiente comando `npm run build.stencil` este comando generara 
los archivos necesarios para que funcione. Despues en la ruta `stencil-library/.storybook/preview-head.html` se tiene que comentar o descomentar ciertas lineas de codigo dependiendo si lo queremos vizualizar en local o cuando vayamos a hacer el build de storybook.

### En local

En el archivo `preview-head.html` debemos dejarlo de la siguiente forma:

```

<!-- Para DEV -->
<link rel="stylesheet" href="../dist/pragma-ds/pragma-ds.css" />

...

<!-- Para DEV -->
<script type="module" src="../dist/pragma-ds/pragma-ds.esm.js"></script>
<script nomodule="" src="../dist/pragma-ds/index.esm.js"></script>

```

y ahora se ejecuta el comando `npm run storybook`.

### Desplegar Storybook

Para desplegar storybook se tiene que hacer los siguientes ajustes en el archivo
`preview-head.html`

```

<!-- Para PDN -->
<link rel="stylesheet" href="./pragma-ds/pragma-ds.css" />

...

<!-- Para PDN -->
<script type="module" src="./pragma-ds/pragma-ds.esm.js"></script>
<script nomodule="" src="./pragma-ds/index.esm.js"></script>

```

Ademas tenemos que hacer el build de la libreria de stencil. Acto seguido ejecutar el siguiente comando: `npm run build.storybook`.

Ahora en la siguiente ruta `stencil-library/dist` copiar la carpeta `pragma-ds` y pegarla en la siguiente carpeta `stencil-library/storybook-static`

Puedes publicar Storybook en cualquier sitio, por ejemplo Netlify
subiendo la carpeta `storybook-static`.




