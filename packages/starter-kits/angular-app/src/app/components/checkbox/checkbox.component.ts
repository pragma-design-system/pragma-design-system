import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent implements OnInit{
  form!: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      acceptTerms: [false, Validators.requiredTrue],
    });
  }

  onSubmit() {
    this.submitted = true;

    if (this.form.invalid)
    {
      return;
    }

    alert('SUCCESS!! \n\n' + JSON.stringify(this.form.value, null, 4));
  }

  change(event: Event) {
    console.log('-> Event change', event);
  }

}
