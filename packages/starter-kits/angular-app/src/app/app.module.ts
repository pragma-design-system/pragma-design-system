import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ComponentLibraryModule } from "pragma-ds-angular";

import { AppComponent } from './app.component';

@NgModule({
  declarations: [ AppComponent ],
  imports: [ BrowserModule, ComponentLibraryModule ],
  bootstrap: [AppComponent],
})
export class AppModule {}
