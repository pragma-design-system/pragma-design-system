export interface ToastComponent {
  show(): void;
  hide(): void;
}
