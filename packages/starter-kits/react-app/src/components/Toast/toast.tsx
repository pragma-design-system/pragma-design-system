

import { useState } from 'react'
import { PdsToast } from '@pragma-dev-kit/pragma-ds-react'

const Toast = () => {
    const [showToast, setShowToast] = useState([false, false, false, false])

    const openToast = (id: number) => {
        const auxToast = [...showToast];
        auxToast[id] = true;
        setShowToast(auxToast);
        setTimeout(() => {
            setShowToast([false, false, false, false]);
        }, 5000);
    }

    return (
        <div>
            <button onClick={() => openToast(0)}>Mostrar Toast</button>
            {showToast[0] && <PdsToast title='Toast desde react' message="¡Hola desde React!" duration={5000} />}

            <button onClick={() => openToast(1)}>Mostrar Toast</button>
            {showToast[1] && <PdsToast type='error' title='Toast desde react' message="¡Hola desde React!" duration={5000} />}

            <button onClick={() => openToast(2)}>Mostrar Toast</button>
            {showToast[2] && <PdsToast type='alert' title='Toast desde react' message="¡Hola desde React!" duration={5000} />}

            <button onClick={() => openToast(3)}>Mostrar Toast</button>
            {showToast[3] && <PdsToast type='information' title='Toast desde react' message="¡Hola desde React!" duration={5000} />}

            {/* Resto del código de tu aplicación */}
        </div>
    )
}

export default Toast
