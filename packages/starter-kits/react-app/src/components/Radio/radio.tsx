import { useState } from "react";
import { PdsRadio } from "@pragma-dev-kit/pragma-ds-react"
import { PdsRadioCustomEvent } from "@pragma-dev-kit/pragma-ds";

const Radio = () => {
    const [groupRadio, setGroupRadio] = useState<{
        [key: string]: {
            value: boolean;
            disabled: boolean;
        };
    }>({
        op1: {
            value: false,
            disabled: false
        },
        op2: {
            value: false,
            disabled: true
        },
        op3: {
            value: true,
            disabled: false
        },
        op4: {
            value: true,
            disabled: true
        },
    });

    const handleUpdateGroupRadio = (propiedad: PdsRadioCustomEvent<{ value: string; selected: boolean; }>) => {
        const { value } = propiedad.detail

        setGroupRadio((prevState) => {
            const newState = { ...prevState };

            Object.keys(newState).forEach((item) => {
                newState[item].value = item === value ? true : !newState[item].disabled ? false : newState[item].value
            });

            return newState;
        });
    }

    return (
        <>
            <h2>Radio component</h2>
            <div style={{ display: 'flex', gap: '20px' }}>

                {Object.entries(groupRadio).map(([key, value]) => (
                    <PdsRadio
                        key={key}
                        value={key}
                        checked={value.value}
                        disabled={value.disabled}
                        onRadioButtonSelected={(event) => handleUpdateGroupRadio(event)}
                    />
                ))}
            </div>
        </>
    )
}

export default Radio
