/* eslint-disable @typescript-eslint/no-explicit-any */
import { useState } from 'react';
import { PdsTooltip } from '@pragma-dev-kit/pragma-ds-react';

const Tooltip = () => {
    const [showTooltip, setShowTooltip] = useState([false, false, false, false]);

    const handleMouseEnter = (idx: any) => {
        const auxTool = [...showTooltip]
        auxTool[idx] = true
        setShowTooltip(auxTool);
    };

    const handleMouseLeave = (idx: any) => {
        const auxTool = [...showTooltip]
        auxTool[idx] = false
        setShowTooltip(auxTool);
    };
    return (
        <>
            <h2>Tooltip</h2>

            <div style={{ background: 'black', color: '#fff', padding: '100px 20px', display: 'flex', justifyContent: 'space-around', fontFamily: 'poppins' }}>

                <div
                    onMouseEnter={() => handleMouseEnter(1)}
                    onMouseLeave={() => handleMouseLeave(1)}
                    style={{ display: 'inline-block', position: 'relative', background: 'red', padding: '14px', borderRadius: '10px' }}
                >
                    Mostrar tooltip abajo
                    {showTooltip[1] &&
                        <PdsTooltip position='top' style={{ position: 'absolute', top: '150%', left: '-25%' }}>
                            Hi, I'm a Tooltip!!
                        </PdsTooltip>
                    }

                </div>
                <div
                    onMouseEnter={() => handleMouseEnter(2)}
                    onMouseLeave={() => handleMouseLeave(2)}
                    style={{ display: 'inline-block', position: 'relative', background: 'red', padding: '14px', borderRadius: '10px' }}
                >
                    Mostrar tooltip izquierda
                    {showTooltip[2] &&
                        <PdsTooltip position='right' style={{ position: 'absolute', right: '110%', top: '0' }}>
                            Hi, I'm a Tooltip!!
                        </PdsTooltip>
                    }

                </div>
                <div
                    onMouseEnter={() => handleMouseEnter(3)}
                    onMouseLeave={() => handleMouseLeave(3)}
                    style={{ display: 'inline-block', position: 'relative', background: 'red', padding: '14px', borderRadius: '10px' }}
                >
                    Mostrar tooltip derecha
                    {showTooltip[3] &&
                        <PdsTooltip position='left' theme='dark' style={{ position: 'absolute', left: '110%', top: '0' }}>
                            Hi, I'm a Tooltip!!
                        </PdsTooltip>
                    }

                </div>
                <div
                    onMouseEnter={() => handleMouseEnter(0)}
                    onMouseLeave={() => handleMouseLeave(0)}
                    style={{ display: 'inline-block', position: 'relative', background: 'red', padding: '14px', borderRadius: '10px' }}
                >
                    Mostrar tooltip arriba
                    {showTooltip[0] &&
                        <PdsTooltip theme='dark' style={{ position: 'absolute', bottom: '150%', left: '-25%' }}>
                            Hi, I'm a Tooltip!!
                        </PdsTooltip>
                    }

                </div>

            </div>
        </>
    )
}

export default Tooltip
