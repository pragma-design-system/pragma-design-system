import { PdsBreadcrumb } from "@pragma-dev-kit/pragma-ds-react"

const Breadcrum = () => {
    const breadcrumbs = [
        { label: 'Home', link: '/' },
        { label: 'Tienda', link: '/tienda' },
        { label: 'Página Actual', link: '/tienda/actual' },
    ];
    return (
        <div>
            <h1>Breadcrumb</h1>
            <PdsBreadcrumb items={breadcrumbs}></PdsBreadcrumb>
        </div>
    )
}

export default Breadcrum
