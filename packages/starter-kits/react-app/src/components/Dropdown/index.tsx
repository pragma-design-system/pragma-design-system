import React from "react";
import {
  PdsDropdown,
  PdsDropdownOption,
} from "@pragma-dev-kit/pragma-ds-react";

const DropDown: React.FC = () => {
  return (
    <PdsDropdown onValueChange={console.log}>
      <PdsDropdownOption>naranja</PdsDropdownOption>
      <PdsDropdownOption>melon</PdsDropdownOption>
      <PdsDropdownOption>piña</PdsDropdownOption>
    </PdsDropdown>
  );
};

export default DropDown;
