/* eslint-disable @typescript-eslint/no-explicit-any */
import { PdsPagination } from "@pragma-dev-kit/pragma-ds-react"

const Pagination = () => {

    const hanldePageChange = (page: any) => {
        console.log("🚀 ~ file: pagination.tsx:6 ~ hanldePageChange ~ page:", page.detail)
    }
    return (
        <div>
            <h1>Pagination</h1>
            <PdsPagination totalItems={10} itemsPerPage={1} currentPage={1} onPageChange={hanldePageChange}></PdsPagination>
        </div>
    )
}

export default Pagination
