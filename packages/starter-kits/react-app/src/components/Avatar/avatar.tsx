import { PdsAvatar } from "@pragma-dev-kit/pragma-ds-react"

const Avatar = () => {
    return (
        <div>
            <h2>Avatar Component</h2>
            <div style={{ display: 'flex', alignItems: 'center', gap: '10px', marginBottom: '20px' }}>
                <PdsAvatar url="https://static1.cbrimages.com/wordpress/wp-content/uploads/2020/05/Ichigo-Kurosaki.jpg" size="small" />
                <PdsAvatar url="https://pm1.narvii.com/6420/a6e753293ba46d2e39b21806e30bc6b49a5be78b_00.jpg" size="m" />
                <PdsAvatar url="https://medias.spotern.com/spots/w640/72/72284-1532336916.jpg" size="large" />
                <PdsAvatar url="https://ankplanet.com/wp-content/uploads/2022/11/season-1.webp" size="x-large" />
            </div>
            <div style={{ display: 'flex', alignItems: 'center', gap: '10px', marginBottom: '20px' }}>
                <PdsAvatar icon="https://gitlab.com/pragma-design-system/pragma-icons/-/raw/main/pds-arrow-left-f.svg" url="https://static1.cbrimages.com/wordpress/wp-content/uploads/2020/05/Ichigo-Kurosaki.jpg" size="small" />
                <PdsAvatar icon="https://gitlab.com/pragma-design-system/pragma-icons/-/raw/main/pds-arrow-up-f.svg" url="https://pm1.narvii.com/6420/a6e753293ba46d2e39b21806e30bc6b49a5be78b_00.jpg" size="m" />
                <PdsAvatar icon="https://gitlab.com/pragma-design-system/pragma-icons/-/raw/main/pds-arrow-down-f.svg" url="https://medias.spotern.com/spots/w640/72/72284-1532336916.jpg" size="large" />
                <PdsAvatar icon="https://gitlab.com/pragma-design-system/pragma-icons/-/raw/main/pds-arrow-right-f.svg" url="https://ankplanet.com/wp-content/uploads/2022/11/season-1.webp" size="x-large" />
            </div>
            <div style={{ display: 'flex', alignItems: 'center', gap: '10px', marginBottom: '20px' }}>
                <PdsAvatar initials="EB" size="small" />
                <PdsAvatar initials="LJ" />
                <PdsAvatar initials="MG" size="large" />
                <PdsAvatar initials="JB" size="x-large" />
            </div>
            <div style={{ display: 'flex', alignItems: 'center', gap: '10px', marginBottom: '20px' }}>
                <PdsAvatar initials="EB" type-initials="text-white" size="small" />
                <PdsAvatar initials="LJ" type-initials="text-white" />
                <PdsAvatar initials="MG" type-initials="text-white" size="large" />
                <PdsAvatar initials="JB" type-initials="text-white" size="x-large" />
            </div>
            <div style={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
                <PdsAvatar initials="EB" type-initials="text-purple-light" size="small" />
                <PdsAvatar initials="LJ" type-initials="text-purple-light" />
                <PdsAvatar initials="MG" type-initials="text-purple-light" size="large" />
                <PdsAvatar initials="JB" type-initials="text-purple-light" size="x-large" />
            </div>

        </div>
    )
}

export default Avatar
