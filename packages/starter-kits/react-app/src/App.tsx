// App.tsx
import "./App.css";
import { defineCustomElements } from "@pragma-dev-kit/pragma-ds-react";
import Avatar from "./components/Avatar/avatar";
import Radio from "./components/Radio/radio";
import Tooltip from "./components/Tooltip/tooltip";
import Toast from "./components/Toast/toast";
import Pagination from "./components/Pagination/pagination";
import Breadcrum from "./components/Breadcrumb/breadcrum";
import DropDown from "./components/Dropdown";

defineCustomElements();

function App() {
  return (
    <div className="App">
      {/* <MyComponent first="Your" last="Name" /> */}
      <h1>List Components</h1>
      <hr />
      <Avatar />
      <hr />
      <Radio />
      <hr />
      <Tooltip />
      <hr />
      <Toast />
      <hr />
      <Pagination />
      <hr />
      <Breadcrum />
      <hr />
      <DropDown />
    </div>
  );
}

export default App;
