
import * as d from './components';

export const DIRECTIVES = [
  d.PdsAccordion,
  d.PdsAvatar,
  d.PdsBreadcrumb,
  d.PdsButton,
  d.PdsCalendar,
  d.PdsCard,
  d.PdsCheckbox,
  d.PdsDropdown,
  d.PdsDropdownOption,
  d.PdsFilter,
  d.PdsIcon,
  d.PdsIconButton,
  d.PdsInput,
  d.PdsLoading,
  d.PdsPagination,
  d.PdsRadio,
  d.PdsSearch,
  d.PdsStepper,
  d.PdsTab,
  d.PdsTables,
  d.PdsTags,
  d.PdsTextarea,
  d.PdsToast,
  d.PdsToggle,
  d.PdsTooltip
];
