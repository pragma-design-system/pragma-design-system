/* tslint:disable */
/* auto-generated angular directive proxies */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, NgZone } from '@angular/core';

import { ProxyCmp, proxyOutputs } from './angular-component-lib/utils';

import { Components } from '@pragma-dev-kit/pragma-ds';


@ProxyCmp({
  inputs: ['disable', 'icon', 'open', 'size', 'text']
})
@Component({
  selector: 'pds-accordion',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['disable', 'icon', 'open', 'size', 'text'],
})
export class PdsAccordion {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface PdsAccordion extends Components.PdsAccordion {}


@ProxyCmp({
  inputs: ['icon', 'initials', 'size', 'typeInitials', 'url']
})
@Component({
  selector: 'pds-avatar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['icon', 'initials', 'size', 'typeInitials', 'url'],
})
export class PdsAvatar {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface PdsAvatar extends Components.PdsAvatar {}


@ProxyCmp({
  inputs: ['disabled', 'items', 'type']
})
@Component({
  selector: 'pds-breadcrumb',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['disabled', 'items', 'type'],
})
export class PdsBreadcrumb {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface PdsBreadcrumb extends Components.PdsBreadcrumb {}


@ProxyCmp({
  inputs: ['disabled', 'href', 'rel', 'size', 'target', 'type', 'variant']
})
@Component({
  selector: 'pds-button',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['disabled', 'href', 'rel', 'size', 'target', 'type', 'variant'],
})
export class PdsButton {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface PdsButton extends Components.PdsButton {}


@ProxyCmp({
})
@Component({
  selector: 'pds-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: [],
})
export class PdsCalendar {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface PdsCalendar extends Components.PdsCalendar {}


@ProxyCmp({
  inputs: ['size']
})
@Component({
  selector: 'pds-card',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['size'],
})
export class PdsCard {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface PdsCard extends Components.PdsCard {}


@ProxyCmp({
  inputs: ['checked', 'disabled', 'iconColor', 'iconName', 'iconSize', 'indeterminate', 'name', 'value']
})
@Component({
  selector: 'pds-checkbox',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['checked', 'disabled', 'iconColor', 'iconName', 'iconSize', 'indeterminate', 'name', 'value'],
})
export class PdsCheckbox {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['pdschange']);
  }
}


import type { CheckboxChangeEventDetail as IPdsCheckboxCheckboxChangeEventDetail } from '@pragma-dev-kit/pragma-ds';

export declare interface PdsCheckbox extends Components.PdsCheckbox {
  /**
   * Emitted when the checked property has changed
as a result of a user action such as a click.
This event will not emit when programmatically
setting the checked property.
   */
  pdschange: EventEmitter<CustomEvent<IPdsCheckboxCheckboxChangeEventDetail>>;
}


@ProxyCmp({
  inputs: ['disabled', 'label', 'placeholder', 'text']
})
@Component({
  selector: 'pds-dropdown',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['disabled', 'label', 'placeholder', 'text'],
})
export class PdsDropdown {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['valueChange']);
  }
}


export declare interface PdsDropdown extends Components.PdsDropdown {

  valueChange: EventEmitter<CustomEvent<string>>;
}


@ProxyCmp({
  inputs: ['theme', 'value']
})
@Component({
  selector: 'pds-dropdown-option',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['theme', 'value'],
})
export class PdsDropdownOption {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface PdsDropdownOption extends Components.PdsDropdownOption {}


@ProxyCmp({
})
@Component({
  selector: 'pds-filter',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: [],
})
export class PdsFilter {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface PdsFilter extends Components.PdsFilter {}


@ProxyCmp({
  inputs: ['color', 'name', 'size']
})
@Component({
  selector: 'pds-icon',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['color', 'name', 'size'],
})
export class PdsIcon {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['iconClick']);
  }
}


export declare interface PdsIcon extends Components.PdsIcon {

  iconClick: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  inputs: ['actionClick', 'disabled', 'name']
})
@Component({
  selector: 'pds-icon-button',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['actionClick', 'disabled', 'name'],
})
export class PdsIconButton {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface PdsIconButton extends Components.PdsIconButton {}


@ProxyCmp({
  inputs: ['aspect', 'disabled', 'help', 'icon', 'iconColor', 'label', 'maxlength', 'placeholder', 'type', 'value'],
  methods: ['setFocus']
})
@Component({
  selector: 'pds-input',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['aspect', 'disabled', 'help', 'icon', 'iconColor', 'label', 'maxlength', 'placeholder', 'type', 'value'],
})
export class PdsInput {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['ponInput', 'ponChange', 'ponIconClick']);
  }
}


import type { InputChangeEventDetail as IPdsInputInputChangeEventDetail } from '@pragma-dev-kit/pragma-ds';

export declare interface PdsInput extends Components.PdsInput {

  ponInput: EventEmitter<CustomEvent<IPdsInputInputChangeEventDetail>>;

  ponChange: EventEmitter<CustomEvent<IPdsInputInputChangeEventDetail>>;

  ponIconClick: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
})
@Component({
  selector: 'pds-loading',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: [],
})
export class PdsLoading {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface PdsLoading extends Components.PdsLoading {}


@ProxyCmp({
  inputs: ['currentPage', 'itemsPerPage', 'totalItems']
})
@Component({
  selector: 'pds-pagination',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['currentPage', 'itemsPerPage', 'totalItems'],
})
export class PdsPagination {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['pageChange']);
  }
}


export declare interface PdsPagination extends Components.PdsPagination {

  pageChange: EventEmitter<CustomEvent<number>>;
}


@ProxyCmp({
  inputs: ['checked', 'disabled', 'value']
})
@Component({
  selector: 'pds-radio',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['checked', 'disabled', 'value'],
})
export class PdsRadio {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['radioButtonSelected']);
  }
}


export declare interface PdsRadio extends Components.PdsRadio {

  radioButtonSelected: EventEmitter<CustomEvent<{ value: string, selected: boolean }>>;
}


@ProxyCmp({
})
@Component({
  selector: 'pds-search',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: [],
})
export class PdsSearch {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface PdsSearch extends Components.PdsSearch {}


@ProxyCmp({
})
@Component({
  selector: 'pds-stepper',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: [],
})
export class PdsStepper {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface PdsStepper extends Components.PdsStepper {}


@ProxyCmp({
})
@Component({
  selector: 'pds-tab',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: [],
})
export class PdsTab {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface PdsTab extends Components.PdsTab {}


@ProxyCmp({
})
@Component({
  selector: 'pds-tables',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: [],
})
export class PdsTables {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface PdsTables extends Components.PdsTables {}


@ProxyCmp({
})
@Component({
  selector: 'pds-tags',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: [],
})
export class PdsTags {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface PdsTags extends Components.PdsTags {}


@ProxyCmp({
  inputs: ['descTxt', 'disableResize', 'labelTxt', 'maxlength', 'placeholderTxt', 'type', 'value'],
  methods: ['setFocus']
})
@Component({
  selector: 'pds-textarea',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['descTxt', 'disableResize', 'labelTxt', 'maxlength', 'placeholderTxt', 'type', 'value'],
})
export class PdsTextarea {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['ponInput', 'ponChange']);
  }
}


export declare interface PdsTextarea extends Components.PdsTextarea {

  ponInput: EventEmitter<CustomEvent<any>>;

  ponChange: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  inputs: ['closeColorIcon', 'closeNameIcon', 'closeSizeIcon', 'colorIcon', 'message', 'nameIcon', 'sizeIcon', 'text', 'type']
})
@Component({
  selector: 'pds-toast',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['closeColorIcon', 'closeNameIcon', 'closeSizeIcon', 'colorIcon', 'message', 'nameIcon', 'sizeIcon', 'text', 'type'],
})
export class PdsToast {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['ponIconClick']);
  }
}


export declare interface PdsToast extends Components.PdsToast {

  ponIconClick: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  inputs: ['checked', 'checkedColor', 'disabled', 'inputId', 'inputName']
})
@Component({
  selector: 'pds-toggle',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['checked', 'checkedColor', 'disabled', 'inputId', 'inputName'],
})
export class PdsToggle {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['pdschange']);
  }
}


export declare interface PdsToggle extends Components.PdsToggle {

  pdschange: EventEmitter<CustomEvent<boolean>>;
}


@ProxyCmp({
  inputs: ['position', 'theme']
})
@Component({
  selector: 'pds-tooltip',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['position', 'theme'],
})
export class PdsTooltip {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface PdsTooltip extends Components.PdsTooltip {}


