import { APP_INITIALIZER, NgModule } from '@angular/core';
import { defineCustomElements } from '@pragma-dev-kit/pragma-ds/loader';

import { DIRECTIVES } from './stencil-generated';
import { BooleanValueAccessor } from './stencil-generated/boolean-value-accessor';

const DECLARATIONS = [...DIRECTIVES, BooleanValueAccessor];

@NgModule({
  declarations: DECLARATIONS,
  exports: DECLARATIONS,
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: () => {
        return defineCustomElements();
      },
    },
  ],
})
export class ComponentLibraryModule {}
