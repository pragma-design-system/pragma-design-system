import { Config } from '@stencil/core';
import { angularOutputTarget } from '@stencil/angular-output-target';
import { reactOutputTarget } from '@stencil/react-output-target';
import { sass } from '@stencil/sass';
import { angularValueAccessorBindings } from './src/wrapper-bindings/angular.bindings';

export const config: Config = {
  namespace: 'pragma-ds',
  globalStyle: 'src/global/global.scss',
  outputTargets: [
    {
      type: 'dist',
      esmLoaderPath: '../loader',
      copy: [{ src: 'assets/PragmaIcons/fonts', dest: 'fonts' }],
    },
    {
      type: 'dist-custom-elements',
    },
    // {
    //   type: 'docs-readme',
    // },
    {
      type: 'docs-json',
      file: 'path/to/docs.json',
    },
    {
      type: 'www',
      serviceWorker: null, // disable service workers
      copy: [{ src: 'assets/PragmaIcons/fonts', dest: 'fonts' }],
    },
    angularOutputTarget({
      componentCorePackage: '@pragma-dev-kit/pragma-ds',
      directivesProxyFile: '../angular-workspace/projects/component-library/src/lib/stencil-generated/components.ts',
      directivesArrayFile: '../angular-workspace/projects/component-library/src/lib/stencil-generated/index.ts',
      valueAccessorConfigs: angularValueAccessorBindings,
    }),
    reactOutputTarget({
      componentCorePackage: '@pragma-dev-kit/pragma-ds',
      proxiesFile: '../react-library/lib/components/stencil-generated/index.ts',
    }),
  ],
  plugins: [sass()],
  extras: {
    enableImportInjection: true,
  },
};
