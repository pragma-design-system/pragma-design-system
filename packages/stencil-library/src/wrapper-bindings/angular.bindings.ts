import { ValueAccessorConfig } from '@stencil/angular-output-target'

export const angularValueAccessorBindings: ValueAccessorConfig[] = [
  {
    elementSelectors: ['pds-checkbox'],
    event: 'pdschange',
    targetAttr: 'checked',
    type: 'boolean',
  }
]
