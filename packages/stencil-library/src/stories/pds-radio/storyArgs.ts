export const argTypesStory = {
  value: {
    name: 'value',
    control: { type: 'string' },
  },
  disabled: {
    name: 'disabled',
    control: { type: 'boolean' },
  },
  checked: {
    name: 'checked',
    control: { type: 'boolean' },
  },
};

export const parametersControls = {
  controls: { include: ['value', 'disabled', 'checked'] },
}