import type { Meta, StoryObj } from '@storybook/html';
import { isDefined } from '../utils/helpers';
import { extractArgTypes } from '@pxtrn/storybook-addon-docs-stencil';
import { argTypesStory, parametersControls } from './storyArgs';

const meta: Meta = {
  title: 'Componentes/Radio',
  argTypes: extractArgTypes('pds-radio'),
};

export default meta;

type Story = StoryObj;

const RadioTemplate = args => `
  <pds-radio${isDefined(args)}></pds-radio>
`;

const VariantsTemplate = args => `
<div style="display: flex; gap: 20px">
${RadioTemplate({...args})}
${RadioTemplate({ disabled: true, checked: "false", ...args })}
${RadioTemplate({ disabled: true, checked: "true", ...args })}
${RadioTemplate({ checked: true, ...args })}
</div>
`;


export const Variants: Story = {
  name: 'Radio button variants',
  render: VariantsTemplate,
  argTypes: argTypesStory,
};

export const Radio: Story = {
  name: 'Radio',
  render: RadioTemplate,
  argTypes: argTypesStory,
  args: {
    value: "check1"
  }
};
export const RadioChecked: Story = {
  name: 'Radio checked',
  render: RadioTemplate,
  argTypes: argTypesStory,
  args: {
    value: "check2",
    checked: true
  }
};

export const RadioDisabled: Story = {
  name: 'Radio disabled',
  render: RadioTemplate,
  argTypes: argTypesStory,
  args: {
    value: "check3",
    disabled: true,
  }
};

export const RadioDisabledChecked: Story = {
  name: 'Radio disabled checked',
  render: RadioTemplate,
  argTypes: argTypesStory,
  args: {
    value: "check4",
    checked: true,
    disabled: true,
  }
};

