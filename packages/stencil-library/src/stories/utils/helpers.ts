export const isDefined = (args, ignore = []) => {
  let attr = '';
  for (const [key, value] of Object.entries(args)) {
    if (ignore.includes(key)) {
      continue;
    } else {
      attr += ` ${key}="${value}"`;
    }
  }
  return attr;
};

Object.defineProperty(String.prototype, 'capitalize', {
  value: function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
  },
  enumerable: false
});
