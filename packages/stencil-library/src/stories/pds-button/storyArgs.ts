export const argTypesStory = {
  variant: {
    name: 'variant',
    control: { type: 'select' },
  },
  size: {
    name: 'size',
    control: { type: 'select' }
  },
  disabled: {
    name: 'disabled',
    control: { type: 'boolean' }
  },
  type: {
    name: 'type',
    control: { type: 'select' },
  },
  href: {
    name: 'href',
    control: { type: 'text' },
  },
  target: {
    name: 'target',
    control: { type: 'select' },
  },
  rel: {
    name: 'rel',
    control: { type: 'object' },
  }
}

export const parametersControls = {
  controls: { include: ['variant', 'size', 'disabled', 'type', 'href', 'target', 'rel'] },
}
