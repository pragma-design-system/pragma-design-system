import type { Meta, StoryObj } from '@storybook/html';
import { isDefined } from '../utils/helpers';
import { extractArgTypes } from '@pxtrn/storybook-addon-docs-stencil';
import { argTypesStory, parametersControls } from './storyArgs';

const meta: Meta = {
  title: 'Componentes/Button',
  // tags: ['autodocs'],
  argTypes: extractArgTypes('pds-button'),
};
export default meta;

type Story = StoryObj;

const ButtonTemplate = args => `<pds-button${isDefined(args)}>${args.variant ? args.variant?.capitalize() : 'Button'}</pds-button>`;

const VariantsTemplate = args => `
${ButtonTemplate({ variant: 'primary', ...args })}
${ButtonTemplate({ variant: 'secondary', ...args })}
${ButtonTemplate({ variant: 'tertiary', ...args })}
`;

const MediumSizeTemplate = args => `
${ButtonTemplate({ variant: 'primary', size: 'medium', ...args })}
${ButtonTemplate({ variant: 'secondary', size: 'medium', ...args })}
${ButtonTemplate({ variant: 'tertiary', size: 'medium', ...args })}
`;

const SmallSizeTemplate = args => `
${ButtonTemplate({ variant: 'primary', size: 'small', ...args })}
${ButtonTemplate({ variant: 'secondary', size: 'small', ...args })}
${ButtonTemplate({ variant: 'tertiary', size: 'small', ...args })}
`;

const DarkButtonsTemplate = args => `
<div class="dark-theme" style="padding: 2rem">
${ButtonTemplate({ ...args, variant: 'primary' })}
${ButtonTemplate({ ...args, variant: 'primary', disabled: true })}
${ButtonTemplate({ ...args, variant: 'secondary' })}
${ButtonTemplate({ ...args, variant: 'secondary', disabled: true })}
${ButtonTemplate({ ...args, variant: 'tertiary' })}
${ButtonTemplate({ ...args, variant: 'tertiary', disabled: true })}
</div>
`;

export const Button: Story = {
  name: 'Button',
  render: ButtonTemplate,
  argTypes: argTypesStory,
  parameters: parametersControls,
};

export const Variants: Story = {
  name: 'Button Variants',
  render: VariantsTemplate,
  argTypes: argTypesStory,
  parameters: parametersControls,
};

export const MediumSize: Story = {
  name: 'Medium Button Size ',
  render: MediumSizeTemplate,
  argTypes: argTypesStory,
  parameters: parametersControls,
};

export const SmallSize: Story = {
  name: 'Small Button Size',
  render: SmallSizeTemplate,
  argTypes: argTypesStory,
  parameters: parametersControls,
};

export const Disabled: Story = {
  name: 'Disabled Buttons',
  render: VariantsTemplate,
  args: {
    disabled: true,
  },
  argTypes: argTypesStory,
  parameters: parametersControls,
};

export const DarkButtons: Story = {
  name: 'Dark buttons',
  render: DarkButtonsTemplate,
};

export const WhiteButtons: Story = {
  name: 'White buttons',
  render: DarkButtonsTemplate,
  args: {
    class: 'white',
  },
};
