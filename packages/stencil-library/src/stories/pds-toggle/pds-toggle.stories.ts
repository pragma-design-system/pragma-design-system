import type { Meta, StoryObj } from '@storybook/html';
import { isDefined } from '../utils/helpers';
import { extractArgTypes } from '@pxtrn/storybook-addon-docs-stencil';
import { argTypesStory } from './storyArgs';

const meta: Meta = {
  title: 'Componentes/Toggle',
  argTypes: extractArgTypes('pds-toggle'),
};

export default meta;

type Story = StoryObj;

const ToggleTemplate = args => `
  <pds-toggle${isDefined(args)}></pds-toggle>
`;

export const Toggle: Story = {
  name: 'Toggle',
  render: ToggleTemplate,
  argTypes: argTypesStory,
  args: {
  checkedColor: '',
  inputId: 'toggle-input',
  inputName: 'toggle-input',
  checked: false,
  disabled: false,
  }
};
export const ToggleChecked: Story = {
  name: 'Toggle checked',
  render: ToggleTemplate,
  argTypes: argTypesStory,
  args: {
  checkedColor: '',
  inputId: 'toggle-input',
  inputName: 'toggle-input',
  checked: true,
  disabled: false,
  }
};
export const ToggleDisabled: Story = {
  name: 'Toggle disabled',
  render: ToggleTemplate,
  argTypes: argTypesStory,
  args: {
  checkedColor: '',
  inputId: 'toggle-input',
  inputName: 'toggle-input',
  checked: false,
  disabled: true,
  }
};
export const ToggleDisabledChecked: Story = {
  name: 'Toggle disabled checked',
  render: ToggleTemplate,
  argTypes: argTypesStory,
  args: {
  checkedColor: '',
  inputId: 'toggle-input',
  inputName: 'toggle-input',
  checked: true,
  disabled: true,
  }
};

