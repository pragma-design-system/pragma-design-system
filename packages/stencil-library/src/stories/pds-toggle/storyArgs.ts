export const argTypesStory = {
  checkedColor: { control: 'string' },
  inputId: { control: 'string' },
  inputName: { control: 'string' },
  checked: { control: 'boolean' },
  disabled: { control: 'boolean' },
};
