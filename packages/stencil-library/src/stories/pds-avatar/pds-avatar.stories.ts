import type { Meta, StoryObj } from '@storybook/html';
import { isDefined } from '../utils/helpers';
import { extractArgTypes } from '@pxtrn/storybook-addon-docs-stencil';
import { argTypesStory } from './storyArgs';

const meta: Meta = {
  title: 'Componentes/Avatar',
  // tags: ['autodocs'],
  argTypes: extractArgTypes('pds-avatar'),
};

export default meta;

type Story = StoryObj;

const AvatarTemplate = args => `
  <pds-avatar${isDefined(args)}></pds-avatar>
`;

export const Avatar: Story = {
  name: 'Avatar',
  render: AvatarTemplate,
  argTypes: argTypesStory,
  args: {
    initials: 'PD',
    typeInitials: 'text-purple',
    size: 'medium',
  }
};

export const WithImage: Story = {
  name: 'Avatar With Image',
  render: AvatarTemplate,
  argTypes: argTypesStory,
  args:{
    url: 'https://example.com/avatar.jpg',
    initials: 'PD',
    typeInitials: 'text-purple',
    size: 'medium',
  }
};

// export const WithIcon: Story = {
//   name: 'Avatar With Icon',
//   render: AvatarTemplate,
//   argTypes: argTypesStory,
//   args:{
//       icon: 'https://example.com/icon.svg',
//       initials: 'PD',
//       typeInitials: 'text-purple',
//       size: 'medium',
//     }
// };
