export const argTypesStory = {
  url: { control: 'text' },
  icon: { control: 'text' },
  initials: { control: 'text' },
  typeInitials: {
    control: { type: 'select' },
    options: ['text-purple', 'text-white', 'text-purple-light'],
  },
  size: {
    control: { type: 'select' },
    options: ['small', 'medium', 'large', 'x-large'],
  },
};
