import type { Meta, StoryObj } from '@storybook/html';
import { isDefined } from '../utils/helpers';
import { extractArgTypes } from '@pxtrn/storybook-addon-docs-stencil';
import { argTypesStory } from './storyArgs';

import listIcons from './pdsIcons.json';

const meta: Meta = {
  title: 'Componentes/Icon',
  // tags: ['autodocs'],
  argTypes: extractArgTypes('pds-icon'),
};

export default meta;

type Story = StoryObj;

const IconTemplate = args => `
  <pds-icon${isDefined(args)}></pds-icon>
`;

export const Icon: Story = {
  name: 'Icon',
  render: IconTemplate,
  argTypes: argTypesStory,
  args: {
    name: 'share-b',
    size: '32px'
  },
};

const AllIconsTemplate = () => {
  let template= [];
  listIcons.Iconos.forEach(icon => {
    template.push(` 
    <div class="card-icon">
      <pds-icon name="${icon}" size="40px"></pds-icon>
      <p>${icon}</p>
    </div>`);
  });
  return template.join(" ") + `
  <style>
  .div-normal {
    margin-top: 36px
  }

  .div-normal > div > div {
    display: grid;
    grid-template-columns: repeat(6, 1fr);
    grid-column-gap: 6px;
    grid-row-gap: 6px;
    font-family: "Poppins";
  }
  .card-icon {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .card-icon p {
    font-weight: 700;
    color: #330072;
  }
  </style>
  `;
};
export const AllIcons: Story = {
  name: 'All Icon',
  render: AllIconsTemplate,
  argTypes: argTypesStory,
};