export const argTypesStory = {
  items: { control: 'array' },
  disabled: { control: 'boolean' },
  type: {
    control: { type: 'select' },
    options: ['standar', 'underline'],
  },
};
