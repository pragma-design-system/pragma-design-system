import type { Meta, StoryObj } from '@storybook/html';
import { isDefined } from '../utils/helpers';
import { extractArgTypes } from '@pxtrn/storybook-addon-docs-stencil';
import { argTypesStory } from './storyArgs';

const meta: Meta = {
  title: 'Componentes/Breadcrumb',
  argTypes: extractArgTypes('pds-breadcrumb'),
};

export default meta;

type Story = StoryObj;

const BreadcrumbTemplate = args => `
  <pds-breadcrumb${isDefined(args)}></pds-breadcrumb>
`;

export const Breadcrumb: Story = {
  name: 'Breadcrumb',
  render: BreadcrumbTemplate,
  argTypes: argTypesStory,
  args: {
    items: [
      {label: "home", link: '/'},
      {label: "productos", link: '/productos'}
    ],
  }
};
