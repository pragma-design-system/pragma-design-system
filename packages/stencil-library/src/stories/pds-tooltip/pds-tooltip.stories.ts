import type { Meta, StoryObj } from '@storybook/html';
import { isDefined } from '../utils/helpers';
import { extractArgTypes } from '@pxtrn/storybook-addon-docs-stencil';
import { argTypesStory } from './storyArgs';

const meta: Meta = {
  title: 'Componentes/Tooltip',
  argTypes: extractArgTypes('pds-tooltip'),
};

export default meta;

type Story = StoryObj;

const TooltipTemplate = args => `
  <pds-tooltip${isDefined(args)}> <div>This is a tooltip content.</div></pds-tooltip>
`;

export const Tooltip: Story = {
  name: 'Tooltip',
  render: TooltipTemplate,
  argTypes: argTypesStory,
  args: {
    position: "botton"
  }
};
export const TooltipUp: Story = {
  name: 'Tooltip top',
  render: TooltipTemplate,
  argTypes: argTypesStory,
  args: {
    position: "top"
  }
};
export const TooltipLeft: Story = {
  name: 'Tooltip left',
  render: TooltipTemplate,
  argTypes: argTypesStory,
  args: {
    position: "left"
  }
};
export const TooltipRight: Story = {
  name: 'Tooltip right',
  render: TooltipTemplate,
  argTypes: argTypesStory,
  args: {
    position: "right"
  }
};

