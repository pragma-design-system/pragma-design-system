export const argTypesStory = {
  name: { control: 'string' },
  position: {
    control: { type: 'select' },
    options: ['top', 'bottom', 'left', 'right'],
  },
};
