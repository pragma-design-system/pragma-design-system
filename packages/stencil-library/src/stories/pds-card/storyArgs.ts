export const argTypesStory = {
  size: {
    control: { type: 'select' },
    options: ['default', 'small', 'large'],
  },
};
