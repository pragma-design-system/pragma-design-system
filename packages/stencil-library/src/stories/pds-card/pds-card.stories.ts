import type { Meta, StoryObj } from '@storybook/html';
import { isDefined } from '../utils/helpers';
import { extractArgTypes } from '@pxtrn/storybook-addon-docs-stencil';
import { argTypesStory } from './storyArgs';

const meta: Meta = {
  title: 'Componentes/Card',
  argTypes: extractArgTypes('pds-card'),
};

export default meta;

type Story = StoryObj;

const CardTemplate = args => `
  <pds-card${isDefined(args)}>
    <h2>Title card</h2>
    <p>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis deserunt similique 
      perferendis modi placeat odit aliquam iure rem ipsa. Vitae quidem dignissimos cum ex debitis 
      perspiciatis modi fuga adipisci tempora!
    </p>
  </pds-card>
`;

export const Card: Story = {
  name: 'Card',
  render: CardTemplate,
  argTypes: argTypesStory,
};
export const CardSmall: Story = {
  name: 'CardSmall',
  render: CardTemplate,
  argTypes: argTypesStory,
  args: {
    size: "small"
  }
};
export const CardLarge: Story = {
  name: 'CardLarge',
  render: CardTemplate,
  argTypes: argTypesStory,
  args: {
    size: "large"
  }
};
