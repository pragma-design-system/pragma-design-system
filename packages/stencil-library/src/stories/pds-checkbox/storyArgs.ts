export const argTypesStory = {
  checked: {
    name: 'checked',
    control: { type: 'boolean' }
  },
  disabled: {
    name: 'disabled',
    control: { type: 'boolean' }
  },
  indeterminate: {
    name: 'indeterminate',
    control: { type: 'boolean' }
  },
  name: {
    name: 'name',
    control: { type: 'text' },
  },
  value: {
    name: 'value',
    control: { type: 'text' },
  },
}

export const parametersControls = {
  controls: { include: ['checked', 'disabled', 'indeterminate', 'name', 'value'] },
}
