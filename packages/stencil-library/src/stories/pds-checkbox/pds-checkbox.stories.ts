import type { Meta, StoryObj } from '@storybook/html';
import { isDefined } from '../utils/helpers';
import { extractArgTypes } from '@pxtrn/storybook-addon-docs-stencil';
import { argTypesStory, parametersControls } from './storyArgs';

const meta: Meta = {
  title: 'Componentes/Checkbox',
  argTypes: extractArgTypes('pds-checkbox'),
};
export default meta;

type Story = StoryObj;

const CheckboxTemplate = args => `<pds-checkbox${isDefined(args)}>Opción 1</pds-checkbox>`;

export const Checkbox: Story = {
  name: 'Checkbox',
  render: CheckboxTemplate,
  args: {
    checked: true
  },
  argTypes: argTypesStory,
  parameters: parametersControls
};

export const Unchecked: Story = {
  name: 'Unchecked',
  render: CheckboxTemplate,
  args: {
    checked: false
  },
  argTypes: argTypesStory,
  parameters: parametersControls
};

export const DisabledUncheck: Story = {
  name: 'Disabled Uncheck',
  render: CheckboxTemplate,
  args: {
    checked: false,
    disabled: true
  },
  argTypes: argTypesStory,
  parameters: parametersControls
};

export const DisabledChecked: Story = {
  name: 'Disabled Checked',
  render: CheckboxTemplate,
  args: {
    checked: true,
    disabled: true
  },
  argTypes: argTypesStory,
  parameters: parametersControls
};
