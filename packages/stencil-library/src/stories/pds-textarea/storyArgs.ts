export const argTypesStory = {
  labelTxt: {
    name: 'labelTxt',
    control: { type: 'text' },
  },
  valueTxt: {
    name: 'valueTxt',
    control: { type: 'text' },
  },
  placeholderTxt: {
    name: 'placeholderTxt',
    control: { type: 'text' },
  },
  descTxt: {
    name: 'descTxt',
    control: { type: 'text' },
  },
  type: {
    control: { type: 'select' },
    options: ['Basic', 'Error', 'Success', 'Disabled'],
  },
  disableResize: {
    control: { type: 'boolean' },

  }
};

export const parametersControls = {
  controls: { include: ['labelTxt', 'valueTxt', 'placeholderTxt', 'descTxt', 'disableResize', 'type'] },
}
