import type { Meta, StoryObj } from '@storybook/html';
import { isDefined } from '../utils/helpers';
import { extractArgTypes } from '@pxtrn/storybook-addon-docs-stencil';
import { argTypesStory, parametersControls } from './storyArgs';

const meta: Meta = {
  title: 'Componentes/Text Area',
  argTypes: extractArgTypes('pds-textarea'),
};

export default meta;

type Story = StoryObj;


const TextAreaTemplate = args => `
  <pds-textarea${isDefined(args)}></pds-textarea>
`;

export const TextArea: Story = {
  name: 'TextArea',
  render: TextAreaTemplate,
  argTypes: argTypesStory,
  args: {
    'label-txt': 'My label',
    'placeholder-txt': 'Write any text here',
    'desc-txt': 'This is a text description',
    'disableResize': false,
  },
  parameters: parametersControls,
};
