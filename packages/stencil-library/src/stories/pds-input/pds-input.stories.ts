import type { Meta, StoryObj } from '@storybook/html';
import { isDefined } from '../utils/helpers';
import { extractArgTypes } from '@pxtrn/storybook-addon-docs-stencil';
import { argTypesStory } from './storyArgs';

const meta: Meta = {
  title: 'Componentes/Input',
  argTypes: extractArgTypes('pds-input'),
};

export default meta;

type Story = StoryObj;

const InputTemplate = args => `
  <pds-input${isDefined(args)}></pds-input>
`;


export const Input: Story = {
    name: 'Input',
    render: InputTemplate,
    argTypes: argTypesStory,
    args: {
        name:"test",
        // label:"Lorem ipsum dolor",
        // icon:"x-b",
        placeholder:"Placeholder text here",
        // help:"Descriptive text",
    },
  };