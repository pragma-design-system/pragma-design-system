export const argTypesStory = {
    label: { control: 'text' },
    icon: { control: 'text' },
    help: { control: 'text' },
    disabled: { control: 'boolean' },
    placeholder: { control: 'text' },
    aspect: { control: 'text' },
    value: { control: 'text' },
  };