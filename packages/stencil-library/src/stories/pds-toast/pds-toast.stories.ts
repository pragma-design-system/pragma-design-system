import type { Meta, StoryObj } from '@storybook/html';
import { isDefined } from '../utils/helpers';
import { extractArgTypes } from '@pxtrn/storybook-addon-docs-stencil';
import { argTypesStory } from './storyArgs';

const meta: Meta = {
  title: 'Componentes/Toast',
  tags: ['autodocs'],
  argTypes: extractArgTypes('pds-toast'),
};

export default meta;

type Story = StoryObj;

const ToastTemplate = args => `
  <pds-toast${isDefined(args)}></pds-toast>
`;

export const Toast: Story = {
  name: 'Toast',
  render: ToastTemplate,
  argTypes: argTypesStory,
  args: {
    text: 'Default Toast',
    message: 'This is a default toast message.',
    duration: 10000,
    type: 'success',
  }
};

export const ToastInformation: Story = {
  name: 'Toast information',
  render: ToastTemplate,
  argTypes: argTypesStory,
  args: {
    text: 'Default Toast',
    message: 'This is a default toast message.',
    duration: 10000,
    type: 'information',
  }
};

export const ToastError: Story = {
  name: 'Toast error',
  render: ToastTemplate,
  argTypes: argTypesStory,
  args: {
    text: 'Default Toast',
    message: 'This is a default toast message.',
    duration: 10000,
    type: 'error',
  }
};

export const ToastAlert: Story = {
  name: 'Toast alert',
  render: ToastTemplate,
  argTypes: argTypesStory,
  args: {
    text: 'Default Toast',
    message: 'This is a default toast message.',
    duration: 10000,
    type: 'alert',
  }
};

