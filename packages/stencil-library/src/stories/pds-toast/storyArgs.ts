export const argTypesStory = {
  text: { control: 'text' },
  message: { control: 'text' },
  duration: { control: 'number' },
  type: {
    control: { type: 'select' },
    options: ['error', 'success', 'alert', 'information'],
  },
};
