import type { Meta, StoryObj } from '@storybook/html';
import { isDefined } from '../utils/helpers';
import { extractArgTypes } from '@pxtrn/storybook-addon-docs-stencil';
import { argTypesStory } from './storyArgs';

const meta: Meta = {
  title: 'Componentes/Pagination',
  argTypes: extractArgTypes('pds-pagination'),
};

export default meta;

type Story = StoryObj;

const PaginationTemplate = args => `
  <pds-pagination${isDefined(args)}></pds-pagination>
`;

export const Pagination: Story = {
  name: 'Pagination',
  render: PaginationTemplate,
  argTypes: argTypesStory,
  args: {
    "total-items": 10,
    'items-per-page':2,
    'current-page':1
  }
};

