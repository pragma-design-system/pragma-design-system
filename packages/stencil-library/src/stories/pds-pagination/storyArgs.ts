export const argTypesStory = {
  totalItems: { control: 'number' },
  itemsPerPage: { control: 'number' },
  currentPage: { control: 'number' },
};
