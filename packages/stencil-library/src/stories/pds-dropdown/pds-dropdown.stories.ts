import type { Meta, StoryObj } from '@storybook/html';
import { isDefined } from '../utils/helpers';
import { extractArgTypes } from '@pxtrn/storybook-addon-docs-stencil';
import { argTypesStory } from './storyArgs';

const meta: Meta = {
  title: 'Componentes/Dropdown',
  argTypes: extractArgTypes('pds-dropdown'),
};

export default meta;

type Story = StoryObj;

const DropdownTemplate = args => `
  <pds-dropdown${isDefined(args)} style="padding-bottom: 100px">
    <pds-dropdown-option theme="dark" value="Apples">Apples</pds-dropdown-option>
    <pds-dropdown-option theme="light" value="Oranges">Oranges</pds-dropdown-option>
  </pds-dropdown>
`;

export const Dropdown: Story = {
  name: 'Dropdown',
  render: DropdownTemplate,
  argTypes: argTypesStory,
  args: {
    placeholder: 'Select a fruit',
    label: 'Fruit',
    text: 'This is going to be your best fruit.'
  }
};

