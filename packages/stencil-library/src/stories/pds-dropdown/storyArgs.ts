export const argTypesStory = {
  placeholder: { control: 'string' },
  label: { control: 'string' },
  text: { control: 'string' },
  disabled: { control: 'string' },
};
