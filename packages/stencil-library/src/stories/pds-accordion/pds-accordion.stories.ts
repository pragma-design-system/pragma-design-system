import type { Meta, StoryObj } from '@storybook/html';
import { isDefined } from '../utils/helpers';
import { extractArgTypes } from '@pxtrn/storybook-addon-docs-stencil';
import { argTypesStory } from './storyArgs';

const meta: Meta = {
  title: 'Componentes/Accordion',
  // tags: ['autodocs'],
  argTypes: extractArgTypes('pds-accordion'),
};

export default meta;

type Story = StoryObj;

const AccordionTemplate = args => `
  <pds-accordion${isDefined(args)}> Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta quos labore quibusdam laborum officiis reiciendis unde dolore inventore aliquid aspernatur a, voluptates
  repudiandae quis ex magni delectus alias, eos quia?.</pds-accordion>
`;

export const Accordion: Story = {
  name: 'Accordion',
  render: AccordionTemplate,
  argTypes: argTypesStory,
  args: {
    text: "Heading",
    icon:"user-b"
  }
};
export const AccordionOpen: Story = {
  name: 'Accordion open default',
  render: AccordionTemplate,
  argTypes: argTypesStory,
  args: {
    text: "Heading",
    icon:"user-b",
    open: true,
  }
};
export const AccordionDisabled: Story = {
  name: 'Accordion disabled',
  render: AccordionTemplate,
  argTypes: argTypesStory,
  args: {
    text: "Heading",
    icon:"user-b",
    disable: true    
  }
};
