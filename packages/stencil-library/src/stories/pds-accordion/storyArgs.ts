export const argTypesStory = {
  text: { control: 'text' },
  open: { control: 'boolean' },
  icon: { control: 'text' },
  disabled: { control: 'boolean' },
  size: {
    control: { type: 'select' },
    options: ['default', 'block'],
  },
};
