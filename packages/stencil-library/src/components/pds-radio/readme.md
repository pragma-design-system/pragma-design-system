# pds-radio



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                                          | Type      | Default     |
| ---------- | ---------- | -------------------------------------------------------------------- | --------- | ----------- |
| `checked`  | `checked`  | Indica si el Radio Button está seleccionado de forma predeterminada. | `boolean` | `false`     |
| `disabled` | `disabled` | Indica si el Radio Button está deshabilitado.                        | `boolean` | `undefined` |
| `value`    | `value`    | Valor del Radio Button.                                              | `string`  | `undefined` |


## Events

| Event                 | Description | Type                                                 |
| --------------------- | ----------- | ---------------------------------------------------- |
| `radioButtonSelected` |             | `CustomEvent<{ value: string; selected: boolean; }>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
