import { Component, Host, h, Prop, Event, EventEmitter, State, Watch } from '@stencil/core';

@Component({
  tag: 'pds-radio',
  styleUrl: 'pds-radio.scss',
  shadow: true,
})
export class PdsRadio {
  /**
   * Valor del Radio Button.
   */
  @Prop({ reflect: true }) value: string;

   /**
   * Indica si el Radio Button está deshabilitado.
   */
  @Prop({ reflect: true }) disabled: boolean;

  /**
   * Indica si el Radio Button está seleccionado de forma predeterminada.
   */
  @Prop({ reflect: true }) checked: boolean = false;
  
  @Event() radioButtonSelected: EventEmitter<{ value: string, selected: boolean }>;

  @State() selected: boolean = false;


  @Watch('checked') valueChange() {
    this.selected = this.checked;
  }

  connectedCallback() {
    this.selected = this.checked;
  }

  handleClick() {
    if (!this.selected && !this.disabled) {
      this.selected = !this.selected;
      this.radioButtonSelected.emit({ value: this.value, selected: this.selected });
    }
  }

  render() {
    const radioClasses = {
      'radio-button': true,
      'radio-button--disabled': this.disabled
    };
    return (
      <Host>
        <div class={radioClasses} onClick={() => this.handleClick()}>
          {this.selected && <div class="radio-button__dot" />}
        </div>
      </Host>
    );
  }

}