import { Component, Host, h, Prop, EventEmitter, Event } from '@stencil/core';

@Component({
  tag: 'pds-icon',
  styleUrl: 'pds-icon.scss',
  shadow: false,
})
export class PdsIcon {
  /**
   * Nombre del icono
   */
  @Prop() name: string;

  /**
   * Tamaño del icono (opcional, valor predeterminado: '24px')
   */
  @Prop({ mutable: true }) size?: string = '16px';

  /**
   * Color del icono (opcional, valor predeterminado: 'black')
   */
  @Prop({ mutable: true }) color?: string;

  @Event() iconClick!: EventEmitter;

  private actionclick = () => {
    this.iconClick.emit()
  } 

  render() {
    return (
      <Host>
        <span
          class={`pds pds-${this.name}`}
          style={{
            fontSize: this.size,
            color: this.color,
          }}
          onClick={() => this.actionclick()}
        ></span>
      </Host>
    );
  }
}
