# pds-icon



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                               | Type     | Default     |
| -------- | --------- | --------------------------------------------------------- | -------- | ----------- |
| `color`  | `color`   | Color del icono (opcional, valor predeterminado: 'black') | `string` | `undefined` |
| `name`   | `name`    | Nombre del icono                                          | `string` | `undefined` |
| `size`   | `size`    | Tamaño del icono (opcional, valor predeterminado: '24px') | `string` | `'16px'`    |


## Events

| Event       | Description | Type               |
| ----------- | ----------- | ------------------ |
| `iconClick` |             | `CustomEvent<any>` |


## Dependencies

### Used by

 - [pds-accordion](../pds-accordion)
 - [pds-checkbox](../pds-checkbox)
 - [pds-dropdown](../pds-dropdown)
 - [pds-input](../pds-input)
 - [pds-pagination](../pds-pagination)
 - [pds-toast](../pds-toast)

### Graph
```mermaid
graph TD;
  pds-accordion --> pds-icon
  pds-checkbox --> pds-icon
  pds-dropdown --> pds-icon
  pds-input --> pds-icon
  pds-pagination --> pds-icon
  pds-toast --> pds-icon
  style pds-icon fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
