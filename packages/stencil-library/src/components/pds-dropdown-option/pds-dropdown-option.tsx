import { Component, h, Prop } from '@stencil/core';

@Component({
  tag: 'pds-dropdown-option',
  styleUrl: 'pds-dropdown-option.scss',
  // shadow: true
})
export class PdsDropdownOption {
  @Prop() value: string;
  @Prop({ reflect: true }) theme: 'light' | 'dark' = 'light';

  render() {
    return (
      <option
        value={this.value}
        class={{
          'option': true,
          'option--dark-theme': this.theme === 'dark',
        }}
      >
        <slot></slot>
      </option>
    );
  }
}
