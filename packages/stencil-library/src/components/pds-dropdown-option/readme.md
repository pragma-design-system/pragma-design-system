# pds-dropdown-option



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type                | Default     |
| -------- | --------- | ----------- | ------------------- | ----------- |
| `theme`  | `theme`   |             | `"dark" \| "light"` | `'light'`   |
| `value`  | `value`   |             | `string`            | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
