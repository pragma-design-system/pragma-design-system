export interface InputChangeEventDetail {
    value?: string | null;
    event?: Event;
}