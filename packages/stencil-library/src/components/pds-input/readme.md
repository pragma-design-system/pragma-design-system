# pds-input



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute     | Description                                                                | Type      | Default     |
| ------------- | ------------- | -------------------------------------------------------------------------- | --------- | ----------- |
| `aspect`      | `aspect`      | Viasual aspect of the input (error, success, default)                      | `string`  | `undefined` |
| `disabled`    | `disabled`    | It indicates if input is disabled or enabled                               | `boolean` | `undefined` |
| `help`        | `help`        | String to help users. Short description of input                           | `string`  | `undefined` |
| `icon`        | `icon`        | Name of the icon. It uses pds-icon as element                              | `string`  | `undefined` |
| `iconColor`   | `icon-color`  | Color                                                                      | `string`  | `undefined` |
| `label`       | `label`       | The label of the input                                                     | `string`  | `undefined` |
| `maxlength`   | `maxlength`   | Cantidad maxima de caracteres.                                             | `number`  | `undefined` |
| `placeholder` | `placeholder` | Placheholder for input                                                     | `string`  | `undefined` |
| `type`        | `type`        | Use this method to set focus on the element in runtime                     | `string`  | `'text'`    |
| `value`       | `value`       | Property to set the value of the input. Devs can set new values in runtime | `string`  | `''`        |


## Events

| Event          | Description | Type                                  |
| -------------- | ----------- | ------------------------------------- |
| `ponChange`    |             | `CustomEvent<InputChangeEventDetail>` |
| `ponIconClick` |             | `CustomEvent<any>`                    |
| `ponInput`     |             | `CustomEvent<InputChangeEventDetail>` |


## Methods

### `setFocus() => Promise<void>`



#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [pds-icon](../pds-icon)

### Graph
```mermaid
graph TD;
  pds-input --> pds-icon
  style pds-input fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
