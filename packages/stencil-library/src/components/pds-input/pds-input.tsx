import { Component, Host, h, Prop, Method, EventEmitter } from '@stencil/core';
import { Event } from '@stencil/core';
import { InputChangeEventDetail } from './input-interface';

@Component({
  tag: 'pds-input',
  styleUrl: 'pds-input.scss',
  shadow: true,
})
export class PdsInput {
  private nativeInput: HTMLInputElement;

  /**
   * The label of the input
   */
  @Prop() label?: string;

  /**
   * Name of the icon. It uses pds-icon as element
   */
  @Prop() icon?: string;
  /**
   * Color
   */
  @Prop() iconColor?: string;

  /**
   * String to help users. Short description of input
   */
  @Prop() help?: string;

  /**
   * It indicates if input is disabled or enabled
   */
  @Prop() disabled?: boolean;

  /**
   * Placheholder for input
   */
  @Prop() placeholder?: string;

  /**
   * Viasual aspect of the input (error, success, default)
   */
  @Prop() aspect: string;

  /**
   * Property to set the value of the input. Devs can set new values in runtime
   */
  @Prop({ mutable: true }) value: string = '';

  /**
  * Cantidad maxima de caracteres.
  */
  @Prop() maxlength?: number;

  /**
   * Use this method to set focus on the element in runtime
   */

  @Prop() type: string = 'text';
  @Method()
  async setFocus() {
    if (this.nativeInput) {
      this.nativeInput.focus();
    }
  }

  @Event() ponInput!: EventEmitter<InputChangeEventDetail>;
  @Event() ponChange!: EventEmitter<InputChangeEventDetail>;
  @Event() ponIconClick!: EventEmitter;

  private emitValueChange(event?: Event) {
    const { value } = this;
    const newValue = value == null ? value : value.toString();
    this.ponChange.emit({ value: newValue, event });
  }

  private emitInputChange(event?: Event) {
    const { value } = this;
    const newValue = value == null ? value : value.toString();
    this.ponInput.emit({ value: newValue, event });
  }

  private onChange = (ev: Event) => {
    this.emitValueChange(ev);
  };

  private onInput(event: Event) {
    const input = (event.target as HTMLInputElement).value;
    if (input) {
      this.value = input || '';
    }
    this.emitInputChange(event);
  }

  private handleIconClick = () => {
    this.ponIconClick.emit();
  };

  render() {
    const classes = [];

    let icon = '';
    if (this.icon) {
      icon = (
        <i>
          <pds-icon name={this.icon} color={this.iconColor} onIconClick={() => this.handleIconClick()}></pds-icon>
        </i>
      );
    }

    let subIcon = '';
    switch (this.aspect) {
      case 'error':
        subIcon = <pds-icon name="x"></pds-icon>;
        classes.push('has-error');
        break;
      case 'success':
        subIcon = <pds-icon name="check"></pds-icon>;
        classes.push('is-ok');
        break;
      default:
        subIcon = '';
    }

    let help = '';
    if (this.help) {
      help = (
        <span class="help">
          {subIcon}
          {this.help}
        </span>
      );
    }

    let disabled = null;
    if (this.disabled) {
      disabled = true;
      classes.push('is-disabled');
    }

    return (
      <Host class={classes.join(',')}>
        <label>
          {this.label}
          <span>
            <input
              ref={input => {
                this.nativeInput = input;
              }}
              type={this.type}
              disabled={disabled}
              placeholder={this.placeholder}
              value={this.value}
              maxLength={this.maxlength}
              onInput={this.onInput.bind(this)}
              onChange={this.onChange}
            ></input>
            {icon}
          </span>
          {help}
        </label>
      </Host>
    );
  }
}
