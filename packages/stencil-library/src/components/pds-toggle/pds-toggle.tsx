import { Component, EventEmitter, Event, Host, Prop, h } from '@stencil/core';

@Component({
  tag: 'pds-toggle',
  styleUrl: 'pds-toggle.scss',
  shadow: true,
})
export class PdsToggle {

  /**
  * Color del Toggle cuando está activado.
  */
  @Prop() checkedColor = '';

  /**
   * ID del elemento de entrada (input).
   */
  @Prop() inputId = '';

  /**
  * Nombre del elemento de entrada (input).
  */
  @Prop() inputName = '';

  /**
 * Estado del Toggle (activado o desactivado).
 */
  @Prop({ reflect: true, mutable: true }) checked: boolean = false;

  /**
  * Indica si el Toggle está deshabilitado.
  */
  @Prop({ reflect: true }) disabled = false;

  @Event({ eventName: 'pdschange' }) pdsChange!: EventEmitter<boolean>;

  private toggleChecked = (ev: any) => {
    ev.preventDefault();
    const target = ev.target as HTMLInputElement;
    this.checked = target.checked;
    this.pdsChange.emit(this.checked);
  };

  render() {
    return (
      <Host>
        <input
          type='checkbox'
          id={this.inputId}
          name={this.inputName}
          disabled={this.disabled}
          onChange={this.toggleChecked}
          checked={this.checked}
        />
      </Host>
    );
  }
}
