# pds-toggle



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description                                 | Type      | Default |
| -------------- | --------------- | ------------------------------------------- | --------- | ------- |
| `checked`      | `checked`       | Estado del Toggle (activado o desactivado). | `boolean` | `false` |
| `checkedColor` | `checked-color` | Color del Toggle cuando está activado.      | `string`  | `''`    |
| `disabled`     | `disabled`      | Indica si el Toggle está deshabilitado.     | `boolean` | `false` |
| `inputId`      | `input-id`      | ID del elemento de entrada (input).         | `string`  | `''`    |
| `inputName`    | `input-name`    | Nombre del elemento de entrada (input).     | `string`  | `''`    |


## Events

| Event       | Description | Type                   |
| ----------- | ----------- | ---------------------- |
| `pdschange` |             | `CustomEvent<boolean>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
