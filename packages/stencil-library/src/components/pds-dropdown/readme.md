# pds-dropdown



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute     | Description                                  | Type      | Default                   |
| ------------- | ------------- | -------------------------------------------- | --------- | ------------------------- |
| `disabled`    | `disabled`    | Indica si el Dropdown está deshabilitado.    | `boolean` | `undefined`               |
| `label`       | `label`       | Etiqueta del Dropdown.                       | `string`  | `undefined`               |
| `placeholder` | `placeholder` | Texto del marcador de posición del Dropdown. | `string`  | `'Placeholder text here'` |
| `text`        | `text`        | Texto descriptivo del Dropdown.              | `string`  | `undefined`               |


## Events

| Event         | Description | Type                  |
| ------------- | ----------- | --------------------- |
| `valueChange` |             | `CustomEvent<string>` |


## Dependencies

### Depends on

- [pds-icon](../pds-icon)

### Graph
```mermaid
graph TD;
  pds-dropdown --> pds-icon
  style pds-dropdown fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
