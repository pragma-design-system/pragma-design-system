import { Component, Host, h, Event, EventEmitter, State, Prop } from '@stencil/core';

@Component({
  tag: 'pds-dropdown',
  styleUrl: 'pds-dropdown.scss',
  shadow: true,
})
export class PdsDropdown {
  /**
   * Texto del marcador de posición del Dropdown.
   */
  @Prop() placeholder: string = 'Placeholder text here';

  /**
   * Etiqueta del Dropdown.
   */
  @Prop() label: string;

  /**
   * Texto descriptivo del Dropdown.
   */
  @Prop() text: string;

  /**
   * Indica si el Dropdown está deshabilitado.
   */
  @Prop() disabled: boolean;

  @Event() valueChange: EventEmitter<string>;
  @State() isOpen: boolean = false;
  @State() selectedValue: string = '';

  handleChange(event: MouseEvent) {
    const selectElement = event.target as HTMLSelectElement;
    this.selectedValue = selectElement.value;
    this.valueChange.emit(this.selectedValue);
    this.isOpen = false;
  }

  toggleDropdown() {
    if (this.disabled) return;
    this.isOpen = !this.isOpen;
  }

  render() {
    return (
      <Host>
        {this.label && <p class="label">{this.label}</p>}
        <div class="dropdown">
          <div class={`dropdown-placeholder ${this.selectedValue ? 'dropdown-placeholder-selected' : ''}`} onClick={() => this.toggleDropdown()}>
            <span class="dropdown-placeholder-text">{this.selectedValue ? this.selectedValue : this.placeholder}</span>

            <div class={`dropdown-icon ${this.isOpen ? ' icon-active' : ''}`}>
              <pds-icon name="chevron-down-b" color="#fff"></pds-icon>
            </div>
          </div>
          <div style={{ 'padding-top': '5px', 'position': 'absolute', 'z-index': '999', 'width': '100%' }}>
            <div class="dropdown-options">
              <div class="dropdown-options-scrollbar">
                {this.isOpen && (
                  <div onClick={event => this.handleChange(event)}>
                    <slot></slot>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        {this.text && <p class="desc-text">{this.text}</p>}
      </Host>
    );
  }
}
