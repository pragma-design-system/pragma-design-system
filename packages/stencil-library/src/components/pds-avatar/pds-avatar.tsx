import { Component, h, Host, Prop, State, ComponentInterface } from '@stencil/core';

@Component({
  tag: 'pds-avatar',
  styleUrl: 'pds-avatar.scss',
  shadow: true,
})
export class PdsAvatar implements ComponentInterface {
  @State() defaultIcon = 'https://gitlab.com/pragma-design-system/pragma-icons/-/raw/main/pds-clock-f.svg';
  @State() defaultAvatar = 'https://gitlab.com/pragma-design-system/pragma-icons/-/raw/main/pds-user-f.svg';

  /**
   * URL de la imagen del avatar.
   */
  @Prop() url: string;

  /**
  * URL del icono a mostrar en el avatar.
   */
  @Prop() icon: string;

  /**
  * Iniciales que se mostrarán en el avatar si no se proporciona `url`.
  */
  @Prop() initials: string = 'PD';

  /**
   * Refleja el tipo de iniciales que se muestran en el avatar.
   */
  @Prop({ reflect: true })
  typeInitials: 'text-purple' | 'text-white' | 'text-purple-light' = 'text-purple';

  /**
  * Refleja el tamaño del avatar.
  */
  @Prop({ reflect: true })
  size: 'small' | 'medium' | 'large' | 'x-large' = 'medium';


  handleIconError = (event: Event) => {
    this.icon = this.defaultIcon;
    event.preventDefault();
  }
  handleAvatarError = (event: Event) => {
    this.url = this.defaultAvatar;
    event.preventDefault();
  }

  render() {
    return (
      <Host>
        {this.renderAvatar()}
        {this.icon && this.renderIcon()}
      </Host>
    );
  }

  private renderAvatar() {
    if (this.url) {
      return <img class="avatar-img" src={this.url} onError={this.handleAvatarError} />;
    } else {
      return <p class="avatar-text">{this.initials}</p>;
    }
  }

  private renderIcon() {
    return <img class="avatar-icon" src={this.icon} onError={this.handleIconError}></img>;
  }

}
