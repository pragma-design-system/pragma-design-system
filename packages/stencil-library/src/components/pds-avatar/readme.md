# pds-avatar



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description                                                         | Type                                                   | Default         |
| -------------- | --------------- | ------------------------------------------------------------------- | ------------------------------------------------------ | --------------- |
| `icon`         | `icon`          | URL del icono a mostrar en el avatar.                               | `string`                                               | `undefined`     |
| `initials`     | `initials`      | Iniciales que se mostrarán en el avatar si no se proporciona `url`. | `string`                                               | `'PD'`          |
| `size`         | `size`          | Refleja el tamaño del avatar.                                       | `"large" \| "medium" \| "small" \| "x-large"`          | `'medium'`      |
| `typeInitials` | `type-initials` | Refleja el tipo de iniciales que se muestran en el avatar.          | `"text-purple" \| "text-purple-light" \| "text-white"` | `'text-purple'` |
| `url`          | `url`           | URL de la imagen del avatar.                                        | `string`                                               | `undefined`     |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
