# pds-toast



<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                                         | Type                                               | Default     |
| ---------------- | ------------------ | --------------------------------------------------- | -------------------------------------------------- | ----------- |
| `closeColorIcon` | `close-color-icon` |                                                     | `string`                                           | `'#fff'`    |
| `closeNameIcon`  | `close-name-icon`  |                                                     | `string`                                           | `'x-b'`     |
| `closeSizeIcon`  | `close-size-icon`  |                                                     | `string`                                           | `'16px'`    |
| `colorIcon`      | `color-icon`       |                                                     | `string`                                           | `'#fff'`    |
| `message`        | `message`          | Mensaje adicional del Toast.                        | `string`                                           | `undefined` |
| `nameIcon`       | `name-icon`        |                                                     | `string`                                           | `'check-b'` |
| `sizeIcon`       | `size-icon`        |                                                     | `string`                                           | `'16px'`    |
| `text`           | `text`             | Titulo del Toast.                                   | `string`                                           | `undefined` |
| `type`           | `type`             | Tipo de Toast (error, success, alert, information). | `"alert" \| "error" \| "information" \| "success"` | `'success'` |


## Events

| Event          | Description | Type               |
| -------------- | ----------- | ------------------ |
| `ponIconClick` |             | `CustomEvent<any>` |


## Dependencies

### Depends on

- [pds-icon](../pds-icon)

### Graph
```mermaid
graph TD;
  pds-toast --> pds-icon
  style pds-toast fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
