import { Component, Host, h, Prop, getElement, Event, EventEmitter } from '@stencil/core';

@Component({
  tag: 'pds-toast',
  styleUrl: 'pds-toast.scss',
  shadow: true,
})
export class PdsToast {
  /**
   * Titulo del Toast.
   */
  @Prop() text: string;

  /**
   * Mensaje adicional del Toast.
   */
  @Prop() message: string;
  @Prop() nameIcon: string = 'check-b';
  @Prop() colorIcon: string = '#fff';
  @Prop() sizeIcon: string = '16px';
  @Prop() closeNameIcon: string = 'x-b';
  @Prop() closeColorIcon: string = '#fff';
  @Prop() closeSizeIcon: string = '16px';
  /**
   * Tipo de Toast (error, success, alert, information).
   */
  @Prop({ reflect: true }) type: 'error' | 'success' | 'alert' | 'information' = 'success';

  @Event() ponIconClick!: EventEmitter;

  private toastElement: HTMLDivElement;

  componentDidLoad() {
    const hostElement = getElement(this);
    this.toastElement = hostElement.shadowRoot.querySelector('.toast') as HTMLDivElement;
  }

  showToast() {
    this.toastElement.style.animation = 'fadeIn 0.6s ease-in-out forwards';
  }
  closeToast() {
    this.toastElement.style.animation = 'fadeOut 0.6s ease-in-out forwards';
  }

  private handleIconClick = () => {
    this.ponIconClick.emit(false);
  };

  render() {
    return (
      <Host>
        <div class="toast">
          <div class="toast__content">
            <span class="toast__icon">
              <pds-icon name={this.nameIcon} color={this.colorIcon} size={this.sizeIcon}></pds-icon>
            </span>
            <div>
              <h1 class="toast-title">{this.text}</h1>
              {this.message && this.message !== '' && <p class="toast-message">{this.message}</p>}
            </div>
          </div>
          <pds-icon class="icon" name={this.closeNameIcon} color={this.closeColorIcon} size={this.closeSizeIcon} onIconClick={() => this.handleIconClick()}></pds-icon>
        </div>
      </Host>
    );
  }
}
