import { Component, Host, h, Element, ComponentInterface, Prop } from '@stencil/core';
import { hasShadowDom } from '../../utils/utils';
import { Attributes, inheritAriaAttributes } from '../../utils/helpers/inheritAttributes';

@Component({
  tag: 'pds-button',
  styleUrl: 'pds-button.scss',
  shadow: true,
})
export class PdsButton implements ComponentInterface {
  private inheritedAttributes: Attributes = {};

  @Element() el!: HTMLElement;

  /**
  * La variante de color del botón
  */
  @Prop({ reflect: true }) variant: 'primary' | 'secondary' | 'tertiary' = 'primary';
  /**
   * El tamaño del botón.
   */
  @Prop({ reflect: true }) size: 'small' | 'medium';
  /**
   * Establece el botón como desactivado (disabled).
   */
  @Prop({ reflect: true }) disabled = false;
  /**
   * El tipo de botón. Usa `"submit"` para enviar datos de formulario nativos.
   */
  @Prop() type: 'submit' | 'reset' | 'button' = 'button';
  /**
   * Establece la URL del enlace. Si se establece, el botón se renderizará como una etiqueta de ancla (a).
   */
  @Prop() href: string | undefined;
  /**
   * Establece el destino del enlace. Se utiliza cuando se establece la propiedad href.
   */
  @Prop() target: '_blank' | '_parent' | '_self' | '_top' = '_self';
  /**
  * Indica el tipo de enlace.
  */
  @Prop() rel: string | undefined;

  private handleClick = (ev: Event) => {
    if (this.disabled) {
      ev.stopPropagation();
      return;
    }

    if (hasShadowDom(this.el)) {
      const form = this.el.closest('form');

      if (form) {
        ev.preventDefault();
        const fakeButton = document.createElement('button');
        fakeButton.type = this.type;
        fakeButton.style.display = 'none';
        form.appendChild(fakeButton);
        fakeButton.click();
        fakeButton.remove();
      }
    }
  };

  componentWillLoad() {
    this.inheritedAttributes = inheritAriaAttributes(this.el);
  }

  render() {
    const { disabled, type, href, rel, target, inheritedAttributes } = this;
    const TagType = href === undefined ? 'button' : ('a' as any);
    const attrs =
      TagType === 'button'
        ? { type }
        : {
            href,
            rel,
            target,
          };

    return (
      <Host onClick={this.handleClick} aria-disabled={disabled ? 'true' : null}>
        <TagType {...attrs} class="button" part="native" disabled={disabled} {...inheritedAttributes}>
          <slot></slot>
        </TagType>
      </Host>
    );
  }
}
