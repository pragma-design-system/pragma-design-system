# pds-button



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                                                                           | Type                                         | Default     |
| ---------- | ---------- | ----------------------------------------------------------------------------------------------------- | -------------------------------------------- | ----------- |
| `disabled` | `disabled` | Establece el botón como desactivado (disabled).                                                       | `boolean`                                    | `false`     |
| `href`     | `href`     | Establece la URL del enlace. Si se establece, el botón se renderizará como una etiqueta de ancla (a). | `string`                                     | `undefined` |
| `rel`      | `rel`      | Indica el tipo de enlace.                                                                             | `string`                                     | `undefined` |
| `size`     | `size`     | El tamaño del botón.                                                                                  | `"medium" \| "small"`                        | `undefined` |
| `target`   | `target`   | Establece el destino del enlace. Se utiliza cuando se establece la propiedad href.                    | `"_blank" \| "_parent" \| "_self" \| "_top"` | `'_self'`   |
| `type`     | `type`     | El tipo de botón. Usa `"submit"` para enviar datos de formulario nativos.                             | `"button" \| "reset" \| "submit"`            | `'button'`  |
| `variant`  | `variant`  | La variante de color del botón                                                                        | `"primary" \| "secondary" \| "tertiary"`     | `'primary'` |


## CSS Custom Properties

| Name                     | Description                                                                                   |
| ------------------------ | --------------------------------------------------------------------------------------------- |
| `--background`           | Fondo del botón.                                                                              |
| `--background-activated` | Fondo del botón cuando se presiona.                                                           |
| `--background-disabled`  | Fondo del botón cuando está deshabilitado.                                                    |
| `--background-hover`     | fondo del botón al pasar el mouse.                                                            |
| `--border`               | Borde del botón.                                                                              |
| `--border-disabled`      | Borde del botón cuando está deshabilitado.                                                    |
| `--border-radius`        | Radio del borde del botón.                                                                    |
| `--box-shadow`           | Sombra del botón.                                                                             |
| `--box-shadow-activated` | Sombra del botón cuando se presiona.                                                          |
| `--box-shadow-disabled`  | Sombra del botón cuando está deshabilitado.                                                   |
| `--box-shadow-hover`     | Sombra del botón al pasar el mouse.                                                           |
| `--color`                | Color del texto del botón.                                                                    |
| `--color-activated`      | Color del texto del botón cuando se presiona.                                                 |
| `--color-disabled`       | Color del texto del botón cuando está deshabilitado.                                          |
| `--color-hover`          | Color del texto del botón cuando se hace hover.                                               |
| `--font`                 | Fuente con el formato: font-style font-variant font-weight font-size/line-height font-family; |
| `--font-family`          | Familia tipográfica del botón.                                                                |
| `--height`               | Altura del botón.                                                                             |
| `--justify`              | Justifica el contenido del botón.                                                             |
| `--letter-spacing`       | Espaciado entre letras del botón.                                                             |
| `--padding-horizontal`   | Relleno superior e inferior del botón..                                                       |
| `--padding-vertical`     | Relleno izquierdo y derecho del botón.                                                        |
| `--text-transform`       | Transformación de texto del botón.                                                            |
| `--transition`           | Transición del botón.                                                                         |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
