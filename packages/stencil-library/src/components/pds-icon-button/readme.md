# pds-icon



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute  | Description | Type       | Default     |
| ------------- | ---------- | ----------- | ---------- | ----------- |
| `actionClick` | --         |             | `Function` | `undefined` |
| `disabled`    | `disabled` |             | `boolean`  | `false`     |
| `name`        | `name`     |             | `string`   | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
