// import "../../assets/style.css";
import { Component, Host, h, Prop } from '@stencil/core';

@Component({
  tag: 'pds-icon-button',
  styleUrl: 'pds-icon-button.scss',
  assetsDirs: ['assets'],
  shadow: false,
})
export class PdsIconButton {
  @Prop() name: string;
  @Prop() disabled: boolean = false;
  @Prop() actionClick: Function;


  private iconRef: HTMLSpanElement;

  handleClick() {
    if (this.disabled) {
     return;
    }
    this.actionClick()
  }

  handleKeyDown(event: KeyboardEvent) {
    if (event.key === 'Enter' || event.key === ' ') {
      this.handleClick();
    }
  }
  componentDidLoad() {
    if (!this.disabled) {
      this.iconRef.setAttribute('tabindex', '0');
    }
  }

  render() {
    if (this.name.endsWith('-b')) return;
    return (
      <Host>
        <span
          class={`pds pds-${this.name} ${this.disabled ? ' disabled' : ''}`}
          onClick={() => this.handleClick()}
          tabIndex={this.disabled ? -1 : 0}
          ref={(el) => (this.iconRef = el)}
          onKeyDown={(event) => this.handleKeyDown(event)}
        >
        </span>
      </Host>
    );
  }
}