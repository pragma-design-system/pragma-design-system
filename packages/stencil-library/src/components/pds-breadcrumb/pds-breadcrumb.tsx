import { Component, Host, h, Prop } from '@stencil/core';

@Component({
  tag: 'pds-breadcrumb',
  styleUrl: 'pds-breadcrumb.scss',
  shadow: true,
})
export class PdsBreadcrumb {

  /**
  * Lista de elementos de Breadcrumb.
  */
  @Prop() items: BreadcrumbItem[]= [
    {label: "Home", link: "/"}
  ];

  /**
  * Tipo de Breadcrumb.
  */
  @Prop() type: 'standar' | 'underline' = 'standar';

  /**
  * Indica si el Breadcrumb está deshabilitado.
  */
  @Prop() disabled: boolean;

  componentDidLoad() {
    console.log(this.items);
    
  }

  render() {
    const lastItemIndex = this.items.length - 1;
    return (
      <Host>
        <nav>
          <ul class={this.disabled ? 'disabled' : ''}>
            {this.items.map((item, index) => (
              <li class={index === lastItemIndex ? this.type === 'underline' ? 'active underline' : 'active' : ''}>
                <a href={item.link}>{item.label}</a>
              </li>
            ))}
          </ul>
        </nav>
      </Host>
    );
  }
}

/**
 * @description Interfaz para representar un elemento de Breadcrumb.
 * @interface
 */
export interface BreadcrumbItem {
  label: string;
  link: string;
}
