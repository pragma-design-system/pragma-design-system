# pds-breadcrumb



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                 | Type                       | Default                                |
| ---------- | ---------- | ------------------------------------------- | -------------------------- | -------------------------------------- |
| `disabled` | `disabled` | Indica si el Breadcrumb está deshabilitado. | `boolean`                  | `undefined`                            |
| `items`    | --         | Lista de elementos de Breadcrumb.           | `BreadcrumbItem[]`         | `[     {label: "Home", link: "/"}   ]` |
| `type`     | `type`     | Tipo de Breadcrumb.                         | `"standar" \| "underline"` | `'standar'`                            |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
