import { Component, Host, h, Prop, State, Event, EventEmitter, Element } from '@stencil/core';

@Component({
  tag: 'pds-pagination',
  styleUrl: 'pds-pagination.scss',
  shadow: true,
})
export class PdsPagination {

  @Element() el!: HTMLElement;

  /**
   * Número total de elementos.
   */
  @Prop() totalItems: number;

  /**
   * Número de elementos por página.
   */
  @Prop() itemsPerPage: number;

  /**
   * Número de página actual.
   */
  @Prop() currentPage: number;

  @State() pageNumbers: number[];

  @Event() pageChange: EventEmitter<number>;

  private calculatePageNumbers() {
    const totalPages = Math.ceil(this.totalItems / this.itemsPerPage);
    const currentPage = this.currentPage;
    const pageNumbers: number[] = [];

    if (totalPages <= 5) {
      for (let i = 1; i <= totalPages; i++) {
        pageNumbers.push(i);
      }
    } else {
      if (currentPage <= 3) {
        for (let i = 1; i <= 3; i++) {
          pageNumbers.push(i);
        }
        pageNumbers.push(0); 
        pageNumbers.push(totalPages);
      } else if (currentPage > totalPages - 3) {
        pageNumbers.push(1);
        pageNumbers.push(0); 
        for (let i = totalPages - 2; i <= totalPages; i++) {
          pageNumbers.push(i);
        }
      } else {
        pageNumbers.push(1);
        pageNumbers.push(0); 
        for (let i = currentPage - 1; i <= currentPage + 1; i++) {
          pageNumbers.push(i);
        }
        pageNumbers.push(0); 
        pageNumbers.push(totalPages);
      }
    }

    this.pageNumbers = pageNumbers;
  }

  private handlePageChange(page: number) {
    if (page !== this.currentPage) {
      this.currentPage = page;
      this.pageChange.emit(this.currentPage);
      this.calculatePageNumbers();
    }
  }

  componentWillLoad() {
    this.calculatePageNumbers();
  }

  render() {
    const totalPages = Math.ceil(+this.totalItems / +this.itemsPerPage);
    const showFirstArrow = +this.currentPage > 1;
    const showLastArrow = +this.currentPage < totalPages;
    return (
      <Host>
        <div class="paginator">

          {showFirstArrow && (
            <div class="icons" onClick={() => this.handlePageChange(this.currentPage - 1)}>
              <pds-icon name='chevron-left-b' class="icon-chevron-left"></pds-icon>
            </div>
          )}

          {this.pageNumbers.map((pageNumber) => {
            if (pageNumber === 0) {
              return <span class="paginator__ellipsis">...</span>;
            }
            return (
              <button
                class={this.currentPage === pageNumber ? 'active' : ''}
                onClick={() => this.handlePageChange(pageNumber)}
              >
                {pageNumber}
              </button>
            );
          })}

          {showLastArrow && (
            <div class="icons" onClick={() => this.handlePageChange(this.currentPage + 1)}>
              <pds-icon name='chevron-right-b' class="icon-chevron-right"></pds-icon>
            </div>
          )}

        </div>
      </Host>
    );
  }

}
