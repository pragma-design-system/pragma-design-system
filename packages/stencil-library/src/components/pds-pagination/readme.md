# pds-pagination



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute        | Description                     | Type     | Default     |
| -------------- | ---------------- | ------------------------------- | -------- | ----------- |
| `currentPage`  | `current-page`   | Número de página actual.        | `number` | `undefined` |
| `itemsPerPage` | `items-per-page` | Número de elementos por página. | `number` | `undefined` |
| `totalItems`   | `total-items`    | Número total de elementos.      | `number` | `undefined` |


## Events

| Event        | Description | Type                  |
| ------------ | ----------- | --------------------- |
| `pageChange` |             | `CustomEvent<number>` |


## Dependencies

### Depends on

- [pds-icon](../pds-icon)

### Graph
```mermaid
graph TD;
  pds-pagination --> pds-icon
  style pds-pagination fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
