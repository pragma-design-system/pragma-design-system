import { Component, Host, h, Prop, State, Watch } from '@stencil/core';

@Component({
  tag: 'pds-accordion',
  styleUrl: 'pds-accordion.scss',
  shadow: true,
})
export class PdsAccordion {

   /**
   * Texto que se muestra en el encabezado del Accordion.
   */
  @Prop() text: string;

  /**
   * Indica si el Accordion está abierto de forma predeterminada.
   */
  @Prop() open: boolean = false;

  /**
   * Indica si el Accordion está deshabilitado.
   */
  @Prop() disable: boolean = false;

   /**
   * Nombre del ícono que se muestra en el encabezado del Accordion.
   */
  @Prop() icon: string;

   /**
   * Tamaño del Accordion.
   */
  @Prop() size: "default" | "block" = "default";

  @State() isOpen: boolean = false;
  @State() activeMove: boolean[] = [false, false];

  connectedCallback() {
    this.isOpen = this.open;
  }

  @Watch('open')
  hanlleOpenChange(newValue: boolean) {
    this.isOpen = newValue;
  }

  toggleAccordion() {
    if (this.disable) return;
    if (this.isOpen) {
      this.activeMove[1] = true
      this.activeMove[0] = false
    } else {
      this.activeMove[0] = true
      this.activeMove[1] = false
    }

    this.isOpen = !this.isOpen
  }

  render() {
    return (
      <Host>
        <section class="accordion">
          <header class="accordion__header" onClick={() => this.toggleAccordion()}>
            <div class="accordion__header-title">
              {this.icon && <pds-icon class="icon-title" name='airplay-b' size="20px" color='white'></pds-icon>}
              <p>{this.text}</p>
            </div>
            <div class={"accordion__header-icon" + `${this.open ? ' icon--open' : ''}` + `${this.activeMove[0] ? ' open-accordion' : ''} ${this.activeMove[1] ? ' close-accordion' : ''}`}>
              <pds-icon name='chevron-down-b' class="icon-chevron"></pds-icon>
            </div>
          </header>
          {(this.isOpen && !this.disable) && (
            <article class="accordion-content">
              <slot></slot>
            </article>
          )}
        </section>
      </Host>
    );
  }

}
