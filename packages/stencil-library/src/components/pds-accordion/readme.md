# pds-accordion



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description                                                     | Type                   | Default     |
| --------- | --------- | --------------------------------------------------------------- | ---------------------- | ----------- |
| `disable` | `disable` | Indica si el Accordion está deshabilitado.                      | `boolean`              | `false`     |
| `icon`    | `icon`    | Nombre del ícono que se muestra en el encabezado del Accordion. | `string`               | `undefined` |
| `open`    | `open`    | Indica si el Accordion está abierto de forma predeterminada.    | `boolean`              | `false`     |
| `size`    | `size`    | Tamaño del Accordion.                                           | `"block" \| "default"` | `"default"` |
| `text`    | `text`    | Texto que se muestra en el encabezado del Accordion.            | `string`               | `undefined` |


## Dependencies

### Depends on

- [pds-icon](../pds-icon)

### Graph
```mermaid
graph TD;
  pds-accordion --> pds-icon
  style pds-accordion fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
