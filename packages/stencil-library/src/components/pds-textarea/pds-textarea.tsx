import { Component, Host, Prop, h, EventEmitter, Event, Method } from '@stencil/core';

@Component({
  tag: 'pds-textarea',
  styleUrl: 'pds-textarea.scss',
  shadow: true,
})


export class PdsTextarea {
  private nativeTextArea: HTMLTextAreaElement;

  /**
     * Texto de etiqueta para el campo de texto.
     */
  @Prop({ reflect: true }) labelTxt: string = "";

  /**
   * Valor del campo de texto.
   */
  @Prop({ reflect: true }) value: string = "";

  /**
   * Texto de marcador de posición para el campo de texto.
   */
  @Prop({ reflect: true }) placeholderTxt: string = "";

  /**
  * Tipo de estilo del campo de texto.
  */
  @Prop({ reflect: true }) type: "Basic" | "Error" | "Success" | "Disabled" = "Basic";

  /**
  * Texto descriptivo para el campo de texto.
  */
  @Prop({ reflect: true }) descTxt: string = "";

   /**
  * Cantidad maxima de caracteres.
  */
  @Prop() maxlength?: number;

  /**
  * Indica si se debe deshabilitar el redimensionamiento.
  */
  @Prop() disableResize?: boolean = false;

  @Method()
  async setFocus() {
    if (this.nativeTextArea) {
      this.nativeTextArea.focus();
    }
  }


  @Event() ponInput!: EventEmitter<any>;
  @Event() ponChange!: EventEmitter<any>;

  isDisableTxt(): boolean {
    return this.type === "Disabled";
  }

  private emitTextAreaChange(event?: Event){
    const { value } = this;
    const newValue = value == null ? value : value.toString();
    this.ponInput.emit({value: newValue, event})
  }

  private onInput(ev: Event){
    const textArea = (ev.target as HTMLTextAreaElement).value
    if (textArea) {
      this.value = textArea || '';
    }
    this.emitTextAreaChange(ev)
  }

  private onChange = (ev: Event) => {
    this.emitValueChange(ev);
  }

  private emitValueChange(event?: Event) {
    const { value } =  this;
    const newValue = value == null ? value : value.toString();
    this.ponChange.emit({value: newValue, event})

  }

  render() {
    return (
      <Host>
        <label htmlFor="txtarea">
          {this.labelTxt}
        </label>
        <div style={{ position: "relative" }}>
          <textarea
            id="txtarea"
            spellcheck="false"
            placeholder={this.placeholderTxt}
            disabled={this.isDisableTxt()}
            value={this.value}
            onInput={this.onInput.bind(this)}
            onChange={this.onChange}
            maxLength={this.maxlength}
          />
          <div class="wrapper">
            <div class="tab" />
          </div>
        </div>
        <span>{this.descTxt}</span>
      </Host>
    );
  }

}
