# pds-textarea



<!-- Auto Generated Below -->


## Properties

| Property         | Attribute         | Description                                           | Type                                            | Default     |
| ---------------- | ----------------- | ----------------------------------------------------- | ----------------------------------------------- | ----------- |
| `descTxt`        | `desc-txt`        | Texto descriptivo para el campo de texto.             | `string`                                        | `""`        |
| `disableResize`  | `disable-resize`  | Indica si se debe deshabilitar el redimensionamiento. | `boolean`                                       | `false`     |
| `labelTxt`       | `label-txt`       | Texto de etiqueta para el campo de texto.             | `string`                                        | `""`        |
| `maxlength`      | `maxlength`       | Cantidad maxima de caracteres.                        | `number`                                        | `undefined` |
| `placeholderTxt` | `placeholder-txt` | Texto de marcador de posición para el campo de texto. | `string`                                        | `""`        |
| `type`           | `type`            | Tipo de estilo del campo de texto.                    | `"Basic" \| "Disabled" \| "Error" \| "Success"` | `"Basic"`   |
| `value`          | `value`           | Valor del campo de texto.                             | `string`                                        | `""`        |


## Events

| Event       | Description | Type               |
| ----------- | ----------- | ------------------ |
| `ponChange` |             | `CustomEvent<any>` |
| `ponInput`  |             | `CustomEvent<any>` |


## Methods

### `setFocus() => Promise<void>`



#### Returns

Type: `Promise<void>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
