# pds-checkbox



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute       | Description                                                                                                                                                                                                                                                  | Type      | Default        |
| --------------- | --------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------- | -------------- |
| `checked`       | `checked`       | If `true`, the checkbox is selected.                                                                                                                                                                                                                         | `boolean` | `false`        |
| `disabled`      | `disabled`      | If `true`, the user cannot interact with the checkbox.                                                                                                                                                                                                       | `boolean` | `false`        |
| `iconColor`     | `icon-color`    |                                                                                                                                                                                                                                                              | `string`  | `'white'`      |
| `iconName`      | `icon-name`     |                                                                                                                                                                                                                                                              | `string`  | `'check-b'`    |
| `iconSize`      | `icon-size`     | Change icn size                                                                                                                                                                                                                                              | `string`  | `'18px'`       |
| `indeterminate` | `indeterminate` | If `true`, the checkbox will visually appear as indeterminate.                                                                                                                                                                                               | `boolean` | `false`        |
| `name`          | `name`          | The name of the control, which is submitted with the form data.                                                                                                                                                                                              | `string`  | `this.inputId` |
| `value`         | `value`         | The value of the checkbox does not mean if it's checked or not, use the `checked` property for that.  The value of a checkbox is analogous to the value of an `<input type="checkbox">`, it's only used when the checkbox participates in a native `<form>`. | `any`     | `'on'`         |


## Events

| Event       | Description                                                                                                                                                              | Type                                          |
| ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------- |
| `pdschange` | Emitted when the checked property has changed as a result of a user action such as a click. This event will not emit when programmatically setting the checked property. | `CustomEvent<CheckboxChangeEventDetail<any>>` |


## Slots

| Slot        | Description                                    |
| ----------- | ---------------------------------------------- |
| `"default"` | The label text to associate with the checkbox. |


## Shadow Parts

| Part          | Description                                       |
| ------------- | ------------------------------------------------- |
| `"container"` | The container for the checkbox mark.              |
| `"label"`     |                                                   |
| `"mark"`      | The checkmark used to indicate the checked state. |


## CSS Custom Properties

| Name                         | Description                                                                                  |
| ---------------------------- | -------------------------------------------------------------------------------------------- |
| `--background`               | Background of the checkbox container                                                         |
| `--background-checked`       | Background of the checkbox container when checked                                            |
| `--background-disabled`      | Background of the checkbox container when disabled                                           |
| `--border`                   | Border style of the checkbox container                                                       |
| `--border-checked`           | Border style of the checkbox container when checked                                          |
| `--border-disabled`          | Border style of the checkbox container when disabled                                         |
| `--border-radius`            | Border radius of the checkbox container                                                      |
| `--box-shadow-hover`         | Box shadow of the checkbox container                                                         |
| `--checkmark-color-disabled` | Color of the checkbox checkmark when disabled                                                |
| `--font`                     | Font with the format: font-style font-variant font-weight font-size/line-height font-family; |
| `--margin-left-label`        | Margin left of the label                                                                     |
| `--size`                     | Size of the checkbox container                                                               |
| `--transition`               | Transition of the checkbox icon                                                              |


## Dependencies

### Depends on

- [pds-icon](../pds-icon)

### Graph
```mermaid
graph TD;
  pds-checkbox --> pds-icon
  style pds-checkbox fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
