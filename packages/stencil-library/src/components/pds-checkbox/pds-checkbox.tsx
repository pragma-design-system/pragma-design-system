import { Component, Host, h, Element, Prop, Event, EventEmitter, ComponentInterface } from '@stencil/core';
import { Attributes, inheritAriaAttributes } from '../../utils/helpers/inheritAttributes';
import { CheckboxChangeEventDetail } from './checkbox-interface';

let id = 0;
/**
 * @slot default - The label text to associate with the checkbox.
 *
 * @part container - The container for the checkbox mark.
 * @part mark - The checkmark used to indicate the checked state.
 */
@Component({
  tag: 'pds-checkbox',
  styleUrl: 'pds-checkbox.scss',
  shadow: true,
})
export class PdsCheckbox implements ComponentInterface {
  private inputId = `pds-checkbox-${id++}`;
  private inputEl?: HTMLInputElement;
  private inheritedAttributes: Attributes = {};

  @Element() el!: HTMLPdsCheckboxElement;

  /**
   * If `true`, the checkbox is selected.
   */
  @Prop({ reflect: true, mutable: true }) checked: boolean = false;
  /**
   * If `true`, the user cannot interact with the checkbox.
   */
  @Prop({ reflect: true }) disabled = false;
  /**
   * The value of the checkbox does not mean if it's checked or not, use the `checked`
   * property for that.
   *
   * The value of a checkbox is analogous to the value of an `<input type="checkbox">`,
   * it's only used when the checkbox participates in a native `<form>`.
   */
  @Prop({ reflect: true, mutable: true }) value: any | null = 'on';
  /**
   * If `true`, the checkbox will visually appear as indeterminate.
   */
  @Prop({ reflect: true, mutable: true }) indeterminate = false;
  /**
   * The name of the control, which is submitted with the form data.
   */
  @Prop() name: string = this.inputId;

  /**
   * Change icn size
   */
  @Prop() iconSize: string = '18px';

  @Prop() iconName: string = 'check-b';
  @Prop() iconColor: string = 'white';



  /**
   * Emitted when the checked property has changed
   * as a result of a user action such as a click.
   * This event will not emit when programmatically
   * setting the checked property.
   */
  @Event({ eventName: 'pdschange' }) pdsChange!: EventEmitter<CheckboxChangeEventDetail>;

  componentDidLoad() {
    if (this.inputEl && this.indeterminate) {
      // indeterminate property does not exist in HTML but is accessible via js
      this.inputEl.indeterminate = true;
    }
  }

  componentWillLoad() {
    this.inheritedAttributes = inheritAriaAttributes(this.el);
  }

  private toggleChecked = (ev: any) => {
    ev.preventDefault();
    const target = ev.target as HTMLInputElement;
    this.indeterminate = false;
    this.checked = target.checked;
    this.pdsChange.emit({
      checked: this.checked,
      value: this.value,
    });
  };

  render() {
    const { inputId, name, checked, value, disabled, el, inheritedAttributes } = this;
    

    return (
      <Host class={{ 'checkbox--checked': checked, 'checkbox--disabled': disabled }}>
        <label htmlFor={inputId} class="checkbox">
          <input
            type="checkbox"
            id={inputId}
            name={name}
            checked={checked}
            value={value}
            disabled={disabled}
            onChange={this.toggleChecked}
            ref={el => (this.inputEl = el)}
            {...inheritedAttributes}
          />
          <div class="checkbox__control" part="container">
            {this.checked ? <pds-icon name={this.iconName} color={this.iconColor} size={this.iconSize}></pds-icon>: null}
          </div>
          <div class={{ 'checkbox__label': true, 'checkbox__label--hidden': el.textContent === '' }} part="label">
            <slot></slot>
          </div>
        </label>
      </Host>
    );
  }
}
