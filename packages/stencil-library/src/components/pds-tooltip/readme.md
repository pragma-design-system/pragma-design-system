# pds-tooltip



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description | Type                                     | Default    |
| ---------- | ---------- | ----------- | ---------------------------------------- | ---------- |
| `position` | `position` |             | `"bottom" \| "left" \| "right" \| "top"` | `'bottom'` |
| `theme`    | `theme`    |             | `"dark" \| "light"`                      | `'light'`  |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
