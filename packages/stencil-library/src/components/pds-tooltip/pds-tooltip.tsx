import { Component, Host, Prop, h, Element } from '@stencil/core';

@Component({
  tag: 'pds-tooltip',
  styleUrl: 'pds-tooltip.scss',
  shadow: true,
})
export class PdsTooltip {
  @Element() el!: HTMLElement;
  @Prop({ reflect: true }) position: 'top' | 'bottom' | 'left' | 'right' = 'bottom';
  @Prop({ reflect: true }) theme: 'light' | 'dark' = 'light';
  render() {
    return (
      <Host>
        <div class="tooltip">
          <div class="arrow"></div>
          <div class="content">
            <slot></slot>
          </div>
        </div>
      </Host>
    );
  }

}
