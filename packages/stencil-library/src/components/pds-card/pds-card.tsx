import { Component, Host, Prop, h } from '@stencil/core';

/**
 * @slot default - Content is placed between the named slots if provided without a slot.
 */
@Component({
  tag: 'pds-card',
  styleUrl: 'pds-card.scss',
  shadow: true,
})
export class PdsCard {

  /**
  * Tamaño del Card.
  */
  @Prop({ reflect: true }) size: 'default' | 'small' | 'large' = 'default'

  render() {
    return (
      <Host>
        <slot></slot>
      </Host>
    );
  }
}
