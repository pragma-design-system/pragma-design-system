# pds-card



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description      | Type                              | Default     |
| -------- | --------- | ---------------- | --------------------------------- | ----------- |
| `size`   | `size`    | Tamaño del Card. | `"default" \| "large" \| "small"` | `'default'` |


## Slots

| Slot        | Description                                                           |
| ----------- | --------------------------------------------------------------------- |
| `"default"` | Content is placed between the named slots if provided without a slot. |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
