import {defineCustomElements} from '../loader';
import { setStencilDocJson, extractArgTypesFactory } from '@pxtrn/storybook-addon-docs-stencil';
import { Title, Primary, Stories, ArgTypes  } from '@storybook/blocks';
import docJson from '../path/to/docs.json';
import React from 'react';
import themes from './theme';

if(docJson) setStencilDocJson(docJson);

defineCustomElements();

/** @type { import('@storybook/html').Preview } */
const preview = {
  parameters: {
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
      hideNoControlsWarning: true,
      parameters: {
        controls: {
          disabled: true,
        }
      }
    },
    backgrounds: { disable: true },
    knobs: {
      disable: true,
    },
    docs: {
      extractArgTypes: extractArgTypesFactory({ dashCase: true }),
      page: () => (
        <>
          <Title />
          <Primary />
          <ArgTypes />
          <Stories />
        </>
      ),
    },
    darkMode: {
      dark: themes.dark,
      light: themes.light,
      darkClass: 'dark-theme',
      lightClass: 'light-theme',
      stylePreview: true
    },
  },
};

export default preview;
