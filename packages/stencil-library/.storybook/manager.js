// .storybook/manager.js

import { addons } from '@storybook/addons';
import themes from './theme';

addons.setConfig({
  panelPosition: 'right',
  theme: themes.light,
});
