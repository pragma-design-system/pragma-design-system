import { create } from '@storybook/theming';
import Light from './logos/light.svg';
import Dark from './logos/dark.svg';

let light = create({
  base: 'light',

  // Typography
  fontBase: '"Poppins", sans-serif',
  // fontCode: 'monospace',

  brandTitle: 'Pragma Design System',
  brandUrl: 'https://example.com',
  brandImage: Light,
  brandTarget: '_self',

  //
  colorPrimary: '#330072',
  colorSecondary: '#6429cd',

  // UI
  appBg: '#F6F7FC',
  appContentBg: '#F6F7FC',
  appBorderColor: '#F6F7FC',
  // appBorderRadius: 4,

  // Text colors
  textColor: '#330072',
  // textInverseColor: '#330072',

  // Toolbar default and active colors
  barTextColor: '#330072',
  barSelectedColor: '#330072',
  barBg: '#F6F7FC',

  // Form colors
  // inputBg: 'red',
  // inputBorder: '#10162F',
  // inputTextColor: '#10162F',
  // inputBorderRadius: 2,
});

let dark = create({
  base: 'dark',

  fontBase: '"Poppins", sans-serif',

  brandTitle: 'Pragma',
  brandUrl: 'https://example.com',
  brandImage: Dark,
  brandTarget: '_self',

  colorPrimary: '#330072',
  colorSecondary: '#6429cd',

  appBg: '#1F0D3F',
  appContentBg: '#1F0D3F',
  appBorderColor: '#1F0D3F',

  textColor: '#FFFFFF',

  barTextColor: '#FFFFFF',
  barSelectedColor: '#FFFFFF',
  barBg: '#1F0D3F',
});

export default { light, dark };
